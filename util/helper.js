import { parseISO } from 'date-fns';
import isBefore from 'date-fns/isBefore';
import isAfter from 'date-fns/isAfter';

// Massage List page sort handler
export const handleMassageListSort = (sortValue, a, b) => {
  if (sortValue === 1 && a.availability && b.availability) {
    const askedDateAvailableA = a.availability.askedDateAvailable;
    const askedDateAvailableB = b.availability.askedDateAvailable;
    const dateA = a.availability.daysAvailable[0];
    const dateB = b.availability.daysAvailable[0];

    if (askedDateAvailableA && askedDateAvailableB) {
      const timeslotsA = a.availability.timeslots.length;
      const timeslotsB = b.availability.timeslots.length;

      if (timeslotsA > timeslotsB) {
        return -1;
      }
      if (timeslotsA < timeslotsB) {
        return 1;
      }
    }

    if (!askedDateAvailableA && !askedDateAvailableB) {
      if (dateA && dateB) {
        if (isBefore(parseISO(dateA), parseISO(dateB))) {
          return -1;
        }
        if (isAfter(parseISO(dateA), parseISO(dateB))) {
          return 1;
        }
      }
      if (dateA && !dateB) {
        return -1;
      }
      if (!dateA && dateB) {
        return 1;
      }
    }
    if (askedDateAvailableA && !askedDateAvailableB) {
      return -1;
    }
    if (!askedDateAvailableA && askedDateAvailableB) {
      return 1;
    }

    return 0;
  }
  if (sortValue === 2 && a.review_count && b.review_count) {
    return b.review_count - a.review_count;
  }
  if (sortValue === 3 && a.distance && b.distance) {
    return a.distance - b.distance;
  }
  if (sortValue === 4 && a.review_count && b.review_count) {
    return a.review_count - b.review_count;
  }
};

// Massage List page sort options
export const MassageListSortList = [
  {
    id: 1,
    value: 'Availability',
  },
  {
    id: 2,
    value: 'Most popular',
  },
  {
    id: 3,
    value: 'Distance',
  },
  {
    id: 4,
    value: 'Customer Review',
  },
];

/* Slider Config */
export const SliderSettings = {
  dots: false,
  infinite: true,
  autoplay: false,
  arrows: false,
  slidesToShow: 6,
  responsive: [
    {
      breakpoint: 576,
      settings: {
        infinite: false,
        centerMode: false,
        centerPadding: '70px',
        slidesToShow: 3,
        touchMove: true,
      },
    },
    {
      breakpoint: 480,
      settings: {
        infinite: false,
        centerMode: false,
        slidesToShow: 3,
        touchMove: true,
        centerPadding: '45px',
      },
    },
    {
      breakpoint: 400,
      settings: {
        infinite: false,
        centerMode: false,
        slidesToShow: 3,
        touchMove: true,
        centerPadding: '30px',
      },
    },
    {
      breakpoint: 360,
      settings: {
        infinite: false,
        centerMode: false,
        slidesToShow: 3,
        touchMove: true,
        centerPadding: '15px',
      },
    },
  ],
};
