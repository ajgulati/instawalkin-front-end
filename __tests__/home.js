import { ThemeProvider } from 'styled-components';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import Button from 'components/Base/Button';
import 'jest-styled-components';
import Home from '../containers/Home';
import { theme } from '../util/config';
import { initialState as appInitialState } from '../redux/app/reducer';
import { initialState as authInitialState } from '../redux/auth/reducer';

const middlewares = [];
const mockStore = configureStore(middlewares);

const getState = {
  app: appInitialState,
  auth: authInitialState,
};

const mockAction = { type: 'MOCK_ACTION' };
const store = mockStore(getState);

/* Styled and Redux Provider */
const ReduxProvider = ({ children, reduxStore }) => (
  <ThemeProvider theme={theme}>
    <Provider store={reduxStore}>{children}</Provider>
  </ThemeProvider>
);

describe('Home', () => {
  it('Home page snapshot', () => {
    store.dispatch(mockAction);

    const actions = store.getActions();
    expect(actions).toEqual([mockAction]);

    const tree = renderer
      .create(
        <ReduxProvider reduxStore={store}>
          <Home />
        </ReduxProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});

describe('Home mount testing', () => {
  it('Home should render without throwing an error', () => {
    const homeWrap = mount(
      <ReduxProvider reduxStore={store}>
        <Home />
      </ReduxProvider>
    );

    expect(homeWrap.find(Button)).toHaveLength(4);
    expect(homeWrap.containsMatchingElement(<Button>Book Now</Button>)).toEqual(
      true
    );
  });
});
