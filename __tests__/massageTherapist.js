import { ThemeProvider } from 'styled-components';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import 'jest-styled-components';
import MassageSpecialists from '../containers/MassageSpecialists';
import MassagePractice from '../containers/MassagePractice';
import MassageSpecialistDetail from '../containers/MassageSpecialists/massageSpecialistDetail';
import { theme } from '../util/config';
import { initialState as appInitialState } from '../redux/app/reducer';
import { initialState as authInitialState } from '../redux/auth/reducer';

const middlewares = [];
const mockStore = configureStore(middlewares);

const getState = {
  app: appInitialState,
  auth: authInitialState,
};

const store = mockStore(getState);

/* Styled and Redux Provider */
const ReduxProvider = ({ children, reduxStore }) => (
  <ThemeProvider theme={theme}>
    <Provider store={reduxStore}>{children}</Provider>
  </ThemeProvider>
);

describe('Massage Therapist', () => {
  it('Massage therapist listing page snapshot', () => {
    const tree = renderer
      .create(
        <ReduxProvider reduxStore={store}>
          <MassageSpecialists />
        </ReduxProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('Massage therapist detail page snapshot', () => {
    const tree = renderer
      .create(
        <ReduxProvider reduxStore={store}>
          <MassageSpecialistDetail />
        </ReduxProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('Massage practice listing page snapshot', () => {
    const tree = renderer
      .create(
        <ReduxProvider reduxStore={store}>
          <MassagePractice />
        </ReduxProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});
