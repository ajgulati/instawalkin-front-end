import { ThemeProvider } from 'styled-components';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { shallow } from 'enzyme';
import 'jest-styled-components';
import IntakeFormListing from '../containers/IntakeForm';
import IntakeFormDetail from '../containers/IntakeForm/intakeFormDetail';
import ConsentForm from '../containers/ConsentForm';
import { theme } from '../util/config';
import { initialState as appInitialState } from '../redux/app/reducer';
import { initialState as authInitialState } from '../redux/auth/reducer';

const middlewares = [];
const mockStore = configureStore(middlewares);

const getState = {
  app: appInitialState,
  auth: authInitialState,
};

const store = mockStore(getState);

/* Styled and Redux Provider */
const ReduxProvider = ({ children, reduxStore }) => (
  <ThemeProvider theme={theme}>
    <Provider store={reduxStore}>{children}</Provider>
  </ThemeProvider>
);

describe('Forms', () => {
  it('Intake Form Listng page snapshot', () => {
    const tree = renderer
      .create(
        <ReduxProvider reduxStore={store}>
          <IntakeFormListing />
        </ReduxProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('Intake Form Detail page snapshot', () => {
    const tree = renderer
      .create(
        <ReduxProvider reduxStore={store}>
          <IntakeFormDetail />
        </ReduxProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('ConsentForm page snapshot', () => {
    const consentFormWrap = shallow(
      <ReduxProvider reduxStore={store}>
        <ConsentForm />
      </ReduxProvider>
    );

    expect(consentFormWrap).toMatchSnapshot();
  });
});
