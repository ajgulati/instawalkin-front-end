import { ThemeProvider } from 'styled-components';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import 'jest-styled-components';
import { mount } from 'enzyme';
import Login from '../containers/Login';
import Signup from '../containers/Signup';
import ResetPassword from '../containers/ResetPassword';
import ForgotPassoword from '../containers/ForgotPassoword';
import Profile from '../containers/Profile';
import RequestDemo from '../containers/RequestDemo';
import VerifyEmail from '../containers/VerifyEmail';
import { theme } from '../util/config';
import { initialState as appInitialState } from '../redux/app/reducer';
import { initialState as authInitialState } from '../redux/auth/reducer';

const middlewares = [];
const mockStore = configureStore(middlewares);

const getState = {
  app: appInitialState,
  auth: authInitialState,
};

const store = mockStore(getState);

/* Styled and Redux Provider */
const ReduxProvider = ({ children, reduxStore }) => (
  <ThemeProvider theme={theme}>
    <Provider store={reduxStore}>{children}</Provider>
  </ThemeProvider>
);

describe('Auth', () => {
  it('Login Snapshot page snapshot', () => {
    const tree = renderer
      .create(
        <ReduxProvider reduxStore={store}>
          <Login />
        </ReduxProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('Signup Snapshot page snapshot', () => {
    const tree = renderer
      .create(
        <ReduxProvider reduxStore={store}>
          <Signup />
        </ReduxProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('Forgot Password Snapshot page snapshot', () => {
    const tree = renderer
      .create(
        <ReduxProvider reduxStore={store}>
          <ForgotPassoword />
        </ReduxProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('Reset Password Snapshot page snapshot', () => {
    const tree = renderer
      .create(
        <ReduxProvider reduxStore={store}>
          <ResetPassword />
        </ReduxProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('Profile Snapshot page snapshot', () => {
    const tree = renderer
      .create(
        <ReduxProvider reduxStore={store}>
          <Profile />
        </ReduxProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('RequestDemo Snapshot page snapshot', () => {
    const tree = renderer
      .create(
        <ReduxProvider reduxStore={store}>
          <RequestDemo />
        </ReduxProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
  it('VerifyEmail Snapshot page snapshot', () => {
    const tree = renderer
      .create(
        <ReduxProvider reduxStore={store}>
          <VerifyEmail />
        </ReduxProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});

describe('Auth mount testing', () => {
  it('Login should render without throwing an error', () => {
    const loginWrap = mount(
      <ReduxProvider reduxStore={store}>
        <Login />
      </ReduxProvider>
    );

    expect(loginWrap.find('h2').text()).toBe('Easiest way to book a massage');
  });
});
