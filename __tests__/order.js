import { ThemeProvider } from 'styled-components';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import 'jest-styled-components';
import PlaceOrder from '../containers/Order';
import OrderDetail from '../containers/OrderDetail';
import OrderHistory from '../containers/OrderHistory';
import ThrivrFormFlowsPage from '../containers/ThrivrFormFlows';

import { theme } from '../util/config';
import { initialState as appInitialState } from '../redux/app/reducer';
import { initialState as authInitialState } from '../redux/auth/reducer';

const middlewares = [];
const mockStore = configureStore(middlewares);

const getState = {
  app: appInitialState,
  auth: authInitialState,
};

const store = mockStore(getState);

/* Styled and Redux Provider */
const ReduxProvider = ({ children, reduxStore }) => (
  <ThemeProvider theme={theme}>
    <Provider store={reduxStore}>{children}</Provider>
  </ThemeProvider>
);

describe('Order', () => {
  it('Place Order Snapshot page snapshot', () => {
    const tree = renderer
      .create(
        <ReduxProvider reduxStore={store}>
          <PlaceOrder />
        </ReduxProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('Order Detail Snapshot page snapshot', () => {
    const tree = renderer
      .create(
        <ReduxProvider reduxStore={store}>
          <OrderDetail />
        </ReduxProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('Order History Snapshot page snapshot', () => {
    const tree = renderer
      .create(
        <ReduxProvider reduxStore={store}>
          <OrderHistory />
        </ReduxProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('ThrivrForm Email Flows Page Snapshot page snapshot', () => {
    const tree = renderer
      .create(
        <ReduxProvider reduxStore={store}>
          <ThrivrFormFlowsPage />
        </ReduxProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});
