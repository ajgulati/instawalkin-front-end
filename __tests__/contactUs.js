import { ThemeProvider } from 'styled-components';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import 'jest-styled-components';
import ContactUs from '../containers/ContactUs';
import { theme } from '../util/config';
import { initialState as appInitialState } from '../redux/app/reducer';
import { initialState as authInitialState } from '../redux/auth/reducer';

const middlewares = [];
const mockStore = configureStore(middlewares);

const getState = {
  app: appInitialState,
  auth: authInitialState,
};

const store = mockStore(getState);

/* Styled and Redux Provider */
const ReduxProvider = ({ children, reduxStore }) => (
  <ThemeProvider theme={theme}>
    <Provider store={reduxStore}>{children}</Provider>
  </ThemeProvider>
);

describe('Contact us', () => {
  it('Contact us page snapshot', () => {
    const tree = renderer
      .create(
        <ReduxProvider reduxStore={store}>
          <ContactUs />
        </ReduxProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});
