import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import Container from 'react-bootstrap/Container';
import PageLoader from 'components/Views/PageLoader';
import ErrorAlert from 'components/ErrorAlert';
import isEmpty from 'lodash.isempty';
import Button from 'components/Base/Button';
import Footer from 'components/Footer';
import Header from 'components/Header';
import { IntakeFormStyleWrapper, OrderStyleWrapper } from './order.style';
import { useWindowWidth } from '@react-hook/window-size';
import { LayoutDiv } from 'components/StyleComponent';
import {
  consentIntakeForm,
  consentCovidForm,
  getBookingDetail,
} from 'redux/app/actions';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default function Booked({
  bookData,
  params,
  hadleShowFormModel,
  modelClose,
}) {
  const loaderCount = useSelector((state) => state.app.loaderCount);
  const appoinmentBooked = useSelector((state) => state.app.appoinmentBooked);
  const thriveFormData = useSelector((state) => state.app.thriveFormData);
  const errorBackend = useSelector((state) => state.auth.error);
  const userProfile = useSelector((state) => state.auth.userProfile);
  const dispatch = useDispatch();
  const router = useRouter();
  let widthWindow = useWindowWidth();

  let [CovidFigureTwo, setCovidFigureTwo] = useState(false);
  let [CovidFigureThree, setCovidFigureThree] = useState(false);
  let [CovidFigureOne, setCovidFigureOne] = useState(false);
  let [IntakeFigureTwo, setIntakeFigureTwo] = useState(false);
  let [IntakeFigureThree, setIntakeFigureThree] = useState(false);
  let [IntakeFigureOne, setIntakeFigureOne] = useState(false);
  let [BookingData, setBookingData] = useState({});
  let [CovidData, setCovidData] = useState({});
  let [IntakeData, setIntakeData] = useState({});

  const handleCheckFormFlow = (FormFlowDetailes) => {
    if (
      FormFlowDetailes.lessThan24 ||
      FormFlowDetailes?.booking?.lessThan24 ||
      FormFlowDetailes?.data?.lessThan24
    ) {
      if (FormFlowDetailes?.['covid-form']?.covid_form) {
        setCovidFigureThree(true);
        setCovidFigureTwo(false);
        setCovidFigureOne(false);
      }
      if (FormFlowDetailes?.['covid-form']?.covid_form === null) {
        setCovidFigureTwo(true);
        setCovidFigureOne(false);
        setIntakeFigureThree(false);
      }

      if (FormFlowDetailes?.['intake-form']?.alreadyConsented) {
        setIntakeFigureOne(true);
        setIntakeFigureThree(false);
        setIntakeFigureTwo(false);
      }
      if (!FormFlowDetailes?.['intake-form']?.alreadyConsented) {
        setIntakeFigureOne(false);
        if (FormFlowDetailes?.['intake-form']?.intakeForm) {
          setIntakeFigureThree(true);
        } else {
          setIntakeFigureTwo(true);
        }
      }
    } else {
      !isEmpty(FormFlowDetailes) && setCovidFigureOne(true);
      setCovidFigureThree(false);
      setCovidFigureTwo(false);
      if (FormFlowDetailes?.['intake-form']?.alreadyConsented) {
        setIntakeFigureOne(true);
        setIntakeFigureThree(false);
        setIntakeFigureTwo(false);
      } else {
        setIntakeFigureOne(false);
        if (FormFlowDetailes?.['intake-form']?.intakeForm) {
          setIntakeFigureThree(true);
        } else {
          setIntakeFigureTwo(true);
        }
      }
    }
  };

  useEffect(() => {
    if (router?.query?.confirmData || router?.query?.detail)
      dispatch(
        getBookingDetail({
          slug: router?.query?.confirmData || router?.query?.detail,
        })
      );
  }, []);

  useEffect(() => {
    setBookingData(appoinmentBooked?.booking || appoinmentBooked?.data);
    setIntakeData(appoinmentBooked?.['intake-form']);
    setCovidData(appoinmentBooked?.['covid-form']);

    if (!isEmpty(thriveFormData) && params) {
      handleCheckFormFlow(thriveFormData);
    } else {
      if (!isEmpty(appoinmentBooked)) handleCheckFormFlow(appoinmentBooked);
    }
  }, [appoinmentBooked, bookData]);

  const handleRedirect = async () => {
    router.push({
      pathname: `/consent-form`,
      query: {
        covid: params
          ? JSON.stringify(thriveFormData?.['covid-form'])
          : JSON.stringify(CovidData),
        type: 'booked',
        booking: params ? params : BookingData?.booking_slug,
        book_data: JSON.stringify({
          therapist_name: bookData
            ? bookData?.therapist_name
            : BookingData?.therapist_name,
          guest: bookData ? bookData?.guest : BookingData?.guest,
        }),
      },
    });
  };

  const handleRedirectNewIntake = async () => {
    if (params) {
      router.push({
        pathname: `/intake-form/new`,
        query: {
          type: 'new',
          intake: thriveFormData?.['intake-form']?.intakeFormAPIUrl,
          covid: JSON.stringify(thriveFormData?.['covid-form']),
          lessThan24: CovidFigureOne,
          auth: false,
          booking: params,
          book_data: JSON.stringify({
            therapist_name: bookData?.therapist_name,
            guest: bookData?.guest,
          }),
        },
      });
    } else {
      router.push({
        pathname: `/intake-form/new`,
        query: {
          type: 'new',
          intake: IntakeData?.intakeFormAPIUrl,
          covid: JSON.stringify(CovidData),
          lessThan24: CovidFigureOne,
          booking: BookingData?.booking_slug,
          book_data: JSON.stringify({
            therapist_name: BookingData?.therapist_name,
            guest: BookingData?.guest,
          }),
        },
      });
    }
  };

  const handleRedirectOrder = async () => {
    let payload = {
      url: params
        ? thriveFormData?.['intake-form']?.intakeFormAPIUrl
        : IntakeData?.intakeFormAPIUrl,
      data: {
        modifier: 'C',
      },
    };
    const { status } = await dispatch(consentIntakeForm(payload));
    if (status) {
      if (!CovidFigureOne) {
        router.push({
          pathname: '/consent-form',
          query: {
            covid: params
              ? JSON.stringify(thriveFormData?.['covid-form'])
              : JSON.stringify(CovidData),
            booking: params ? params : BookingData?.booking_slug,
            lessThan24: CovidFigureOne,
            book_data: JSON.stringify({
              therapist_name: bookData
                ? bookData?.therapist_name
                : BookingData?.therapist_name,
              guest: bookData ? bookData?.guest : BookingData?.guest,
            }),
          },
        });
      } else {
        dispatch(
          getBookingDetail({
            slug: router?.query?.confirmData || router?.query?.detail,
          })
        );
        router.push(
          `/order-detail/[detail]`,
          `/order-detail/${params ? params : BookingData?.booking_slug}`
        );
      }
    }
  };

  const handleRedirectIntakeUpdate = async () => {
    if (params) {
      const query = new URLSearchParams(
        thriveFormData?.['intake-form']?.intakeFormAPIUrl
      );
      sessionStorage.clear();
      sessionStorage.setItem(
        params,
        JSON.stringify(thriveFormData?.['intake-form']?.intakeForm ?? {})
      );
      router.push({
        pathname: `/intake-form/${query.get('form')}`,
        query: {
          intake: thriveFormData?.['intake-form']?.intakeFormAPIUrl,
          covid: JSON.stringify(thriveFormData?.['covid-form']),
          auth: false,
          lessThan24: CovidFigureOne,
          booking: params,
          book_data: JSON.stringify({
            therapist_name: bookData?.therapist_name,
            guest: bookData?.guest,
          }),
        },
      });
    } else {
      const query = new URLSearchParams(IntakeData?.intakeFormAPIUrl);
      router.push({
        pathname: `/intake-form/${query.get('form')}`,
        query: {
          intake: IntakeData?.intakeFormAPIUrl,
          covid: JSON.stringify(CovidData),
          booking: BookingData?.booking_slug,
          lessThan24: CovidFigureOne,
          book_data: JSON.stringify({
            therapist_name: BookingData?.therapist_name,
            guest: BookingData?.guest,
          }),
        },
      });
    }
  };

  const handleRedirectOrderCovid = async () => {
    let payload = {
      url: params
        ? thriveFormData?.['covid-form']?.covidFormAPIUrl
        : CovidData?.covidFormAPIUrl,
      data: {
        modifier: 'C',
      },
    };
    const { status } = await dispatch(consentCovidForm(payload));
    if (status) {
      if (isEmpty(userProfile)) {
        router.push('/');
        return;
      }
      router.push(
        `/order-detail/[detail]`,
        `/order-detail/${params ? params : BookingData?.booking_slug}`
      );
      modelClose && hadleShowFormModel();
    }
  };
  const handleBackClick = () => {
    router.push(
      `/order-detail/[detail]`,
      `/order-detail/${params ? params : BookingData?.booking_slug}`
    );
  };

  return (
    <PageLoader visible={loaderCount <= 0 ? false : true}>
      <div className="content-wrapper">
        <Head>
          <title>Place Order - Thrivr</title>
        </Head>

        {/* Header Section */}
        {router.pathname === '/booked' && <Header className="header-white" />}

        {/* Banner Section */}
        <LayoutDiv
          style={{
            minHeight: '0px',
          }}
        >
          <IntakeFormStyleWrapper
            toppadding={router.pathname === '/booked' ? 120 : 0}
            minHeight={router.pathname === '/booked' ? true : false}
          >
            <OrderStyleWrapper
              is_order_screen={router.pathname === '/booked' ? false : true}
            >
              <Container>
                {/* Error Alert */}
                {errorBackend && typeof errorBackend === 'string' && (
                  <ErrorAlert message={errorBackend} />
                )}
                <div className="order-confirm-wrapper">
                  {router.pathname === '/booked' && (
                    <a onClick={() => handleBackClick()} className="back-arrow">
                      <img width="28" height="28" src="/images/left.svg" />
                    </a>
                  )}

                  <div className="order-header">
                    {router?.query?.confirmData && (
                      <h4 className="booked-heder-bold">
                        Your massage has been booked
                      </h4>
                    )}
                    {IntakeFigureTwo && (
                      <div className="booked-main-div">
                        <Row>
                          <Col sm={6}>
                            <h5>Intake Form</h5>
                          </Col>
                          {widthWindow <= 575 && (
                            <Col sm={10} className="confirm-content-pd">
                              Please fill this for your therapist
                            </Col>
                          )}
                          <Col sm={3}>
                            {' '}
                            <Button
                              className="btn d-sm-block"
                              onClick={handleRedirectNewIntake}
                            >
                              Fill
                            </Button>
                          </Col>
                        </Row>
                        {widthWindow > 575 && (
                          <Row>
                            <Col sm={10} className="confirm-content-pd">
                              Please fill this for your therapist
                            </Col>
                          </Row>
                        )}
                      </div>
                    )}
                    {IntakeFigureThree && (
                      <div className="booked-main-div">
                        <Row>
                          <Col sm={6}>
                            <h5>Intake Form</h5>
                          </Col>
                          {widthWindow <= 575 && (
                            <Col sm={10} className="confirm-content-pd">
                              Please fill or give therapist consent to your old
                              form
                            </Col>
                          )}
                          <Col sm={6} className="booked-nested-div">
                            {' '}
                            <Button
                              className="Btnbooked"
                              onClick={handleRedirectOrder}
                            >
                              Consent
                            </Button>
                            <Button
                              className="btn-hight-booked"
                              onClick={handleRedirectIntakeUpdate}
                            >
                              Update
                            </Button>{' '}
                          </Col>
                        </Row>
                        {widthWindow > 575 && (
                          <Row>
                            <Col sm={10} className="confirm-content-pd">
                              Please fill or give therapist consent to your old
                              form
                            </Col>
                          </Row>
                        )}
                      </div>
                    )}
                    {IntakeFigureOne && (
                      <div className="booked-main-div">
                        <Row>
                          <Col sm={6}>
                            <h5>Intake Form</h5>
                          </Col>
                          {widthWindow <= 575 && (
                            <Col sm={10} className="confirm-content-pd">
                              A form is filled for this therapist. You can
                              update if you like.
                            </Col>
                          )}
                          <Col sm={3}>
                            {' '}
                            <Button
                              className="btn d-sm-block"
                              onClick={handleRedirectIntakeUpdate}
                            >
                              Update
                            </Button>
                          </Col>
                        </Row>
                        {widthWindow > 575 && (
                          <Row>
                            <Col sm={10} className="confirm-content-pd">
                              A form is filled for this therapist. You can
                              update if you like.
                            </Col>
                          </Row>
                        )}
                      </div>
                    )}
                    {CovidFigureTwo && (
                      <div className="booked-main-div">
                        <Row>
                          <Col sm={6}>
                            <h5>Covid Form</h5>
                          </Col>
                          {widthWindow <= 575 && (
                            <Col sm={10} className="confirm-content-pd">
                              Please fill this for your therapist
                            </Col>
                          )}
                          <Col sm={3}>
                            {' '}
                            <Button
                              className="btn d-sm-block"
                              onClick={handleRedirect}
                            >
                              Fill
                            </Button>
                          </Col>
                        </Row>
                        {widthWindow > 575 && (
                          <Row>
                            <Col sm={10} className="confirm-content-pd">
                              Please fill this for your therapist
                            </Col>
                          </Row>
                        )}
                      </div>
                    )}
                    {CovidFigureThree && (
                      <div className="booked-main-div">
                        <Row>
                          <Col sm={6}>
                            <h5>Covid Form</h5>
                          </Col>
                          {widthWindow <= 575 && (
                            <Col sm={10} className="confirm-content-pd">
                              Please fill or give therapist consent to your old
                              form
                            </Col>
                          )}
                          <Col sm={3} className="booked-nested-div">
                            {' '}
                            <Button
                              className="Btnbooked"
                              onClick={handleRedirect}
                            >
                              Fill
                            </Button>
                            <Button
                              className="btn-hight-booked"
                              onClick={handleRedirectOrderCovid}
                            >
                              Consent
                            </Button>
                          </Col>
                        </Row>
                        {widthWindow > 575 && (
                          <Row>
                            <Col sm={10} className="confirm-content-pd">
                              Please fill or give therapist consent to your old
                              form
                            </Col>
                          </Row>
                        )}
                      </div>
                    )}
                    {CovidFigureOne && (
                      <Row>
                        <Col sm={6}>
                          <h5>Covid Form</h5>
                        </Col>
                        <Col sm={10} className="confirm-content-pd">
                          You can only fill this form 24hrs before the
                          appoinment
                        </Col>
                      </Row>
                    )}
                  </div>
                </div>
              </Container>
            </OrderStyleWrapper>
          </IntakeFormStyleWrapper>
        </LayoutDiv>

        {/* Footer Section */}
        {router.pathname === '/booked' && <Footer />}
      </div>
    </PageLoader>
  );
}
