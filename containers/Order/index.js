import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import * as yup from 'yup';
import Form from 'react-bootstrap/Form';
import { useForm, Controller } from 'react-hook-form';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import Container from 'react-bootstrap/Container';
import PageLoader from 'components/Views/PageLoader';
import isEmpty from 'lodash.isempty';
import Button from 'components/Base/Button';
import Input from 'components/Base/Input';
import InputGroup from 'components/Base/InputGroup';
import { SeperatorStyle } from 'components/StyleComponent';
import Footer from 'components/Footer';
import Header from 'components/Header';
import { OrderStyleWrapper } from './order.style';
import { motion, AnimatePresence } from 'framer-motion';
import Alert from 'react-bootstrap/Alert';
import CustomDropdown from 'components/Base/Dropdown';
import InputMask from 'react-input-mask';
import CustomModal from 'components/Base/Modal';
import CustomChipBanner from 'components/Base/ChipBanner';
import ErrorAlert from 'components/ErrorAlert';
import {
  getProjectPricingPerTherapist,
  getTherapistData,
  getTherapistAvailability,
  bookMassageAppoinment,
  createBookingGuest,
  getBookingGuests,
  getValidDiscount,
  consentIntakeForm,
  getPromoCode,
} from 'redux/app/actions';

import {
  getUserProfileData,
  getCreditCards,
  addCard,
  userEmailUpdate,
  profileUpdate,
  getRewardData,
} from 'redux/auth/actions';
import {
  phoneRegex,
  cardRegex,
  emailRegex,
  timezoneRegexFormat,
} from 'util/config';
import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';
import { StripeCreditCardInput } from 'components/StripeComponents/creditCardInput';
import { Accordion, Card, useAccordionToggle } from 'react-bootstrap';
import classNames from 'classnames';

const EmailPhoneschema = yup.object().shape({
  actualEmail: yup.string(),
  actualPhone: yup.string(),
  email: yup
    .string()
    .when('actualEmail', {
      is: null,
      then: yup.string().required('Must enter email address'),
    })
    .matches(emailRegex, 'Please enter valid email'),
  phone: yup
    .string()
    .when('actualPhone', {
      is: null,
      then: yup.string().required('Must enter phone number'),
    })
    .matches(phoneRegex, 'Please enter valid phone number'),
});

const Cardschema = yup.object().shape({
  card_name: yup
    .string()
    .required('Please enter the card number')
    .matches(cardRegex, 'Please enter valid card number'),
  card_date: yup
    .string()
    .required('Please enter the date')
    .matches(
      /^(1[0-2]|0[1-9]|\d)\/([2-9]\d[1-9]\d|[1-9]\d)$/,
      'Please enter valid date'
    ),
  card_cvv: yup
    .string()
    .required('Please enter the cvv')
    .matches(/^[0-9]{3,4}$/, 'Please enter valid card cvv'),
});

export default function PlaceOrder() {
  const loaderCount = useSelector((state) => state.app.loaderCount);
  const therapistPerAvailability = useSelector(
    (state) => state.app.therapistPerAvailability
  );
  const therapistIndividualData = useSelector(
    (state) => state.app.therapistIndividual
  );

  const guestList = useSelector((state) => state.app.guestList);
  const errorBackend = useSelector((state) => state.app.error);
  const userProfile = useSelector((state) => state.auth.userProfile);
  const creditCards = useSelector((state) => state.auth.creditCards);
  const userRewards = useSelector((state) => state.auth.userRewards);
  const dispatch = useDispatch();
  const router = useRouter();
  const errorBackend2 = useSelector((state) => state.auth.error);

  const isLoggedIn = isEmpty(userProfile) ? false : true;
  let [modalShow, setModalShow] = useState(false);
  let [therapistIndividual, setTherapistIndividual] = useState(0);
  let [projectIdSpecific, setProjectIdSpecific] = useState(0);
  let [typeIdSpecific, setTypeIdSpecific] = useState(0);
  let [projectPricing, setProjectPricing] = useState([]);
  let [projectPrice, setPricing] = useState([]);
  let [dateValue, setDateTime] = useState(new Date());
  let [specialistValue, setSpecialistArr] = useState([]);
  let [creditCardData, setCreditCardData] = useState([]);
  let [currentCard, setCurrentCard] = useState(null);
  let [discountValid, setDiscountValid] = useState(false);
  let [currentPaymentType, setCurrentPaymentType] = useState(null);
  let [availabilityData, setAvailabilityData] = useState({});
  let [email, setEmail] = useState();
  let [phone, setPhone] = useState();
  let [cancelModalShow, setCancelModalShow] = useState(false);
  let [bookingNameModalShow, setBookingNameModalShow] = useState(false);
  let [bookingName, setBookingName] = useState();
  let [defaultName, setDefaultName] = useState([]);
  let [guestData, setGuestData] = useState([]);
  let [currentGuest, setCurrentGuest] = useState(0);
  let [modalConfirmShow, setModalConfirmShow] = useState(false);
  let [confirmData, setConfirmData] = useState();
  let [forceCreditCardCreation, setForceCreditCardCreation] = useState(false);
  // Stripes elements
  const stripe = useStripe();
  const elements = useElements();

  const {
    register,
    handleSubmit: emailPhoneHandleSubmit,
    control,
    errors,
    setValue,
    setError,
  } = useForm({
    validationSchema: EmailPhoneschema,
    defaultValues: {
      email: '',
      phone: '',
      actualEmail: null,
      actualPhone: null,
    },
  });

  let [promoCodeValue, setPromoCodeValue] = useState('');
  let [promoCodeData, setPromoCodeData] = useState({});
  let [loginError, setIsNotLoginError] = useState(false);
  let [showPromoCodeForm, setShowPromoCodeForm] = useState(false);

  // Add Credit Card Form

  //   Mount
  useEffect(() => {
    if (!isEmpty(router) && !isEmpty(router.query)) {
      if (!isEmpty(router.query.order)) {
        fetchData(router.query.order);
      }
    }
    fetchUserData();
  }, []);

  useEffect(() => {
    if (!isEmpty(therapistIndividualData)) {
      setTherapistIndividual(
        Array.isArray(therapistIndividualData?.profileSlotsData)
          ? therapistIndividualData?.profileSlotsData?.[0]
          : therapistIndividualData?.profileSlotsData
      );
    }
  }, [therapistIndividualData]);

  // Get Credit Card
  useEffect(() => {
    let creditList = [];
    let paymentlistToUse = PaymentTypeListWithDB;
    if (!isEmpty(therapistIndividual)) {
      if (!therapistIndividual?.direct_billing) {
        paymentlistToUse = PaymentTypeList;
      }

      if (!isEmpty(creditCards)) {
        creditCards.map((element) => {
          creditList.push({
            id: element.id,
            value: `${element.card_brand} ${element.card_last_four}`,
          });
          if (element.default_card && !currentPaymentType) {
            setCurrentCard(element.id);
            setCurrentPaymentType(element.id);
          }
        });
        setCreditCardData([...creditList, ...paymentlistToUse]);
      } else {
        setCreditCardData([...paymentlistToUse]);
      }
    }
  }, [creditCards, therapistIndividual]);

  useEffect(() => {
    let guest = [];
    if (!isEmpty(guestList)) {
      guestList.map((element) => {
        guest.push({
          id: element.id,
          value: element.fullname,
        });
      });
      setGuestData([...guest, ...defaultName]);
    } else {
      setGuestData([]);
    }
  }, [guestList]);

  useEffect(() => {
    if (!isEmpty(userProfile)) {
      let dataUser = {};
      dataUser.id = 0;
      dataUser.value = `${userProfile.firstname} ${userProfile.lastname}`;
      setDefaultName([dataUser]);
      setCurrentGuest(0);
      setValue('actualEmail', userProfile.email ? userProfile.email : null);
      setValue('actualPhone', userProfile.phone ? userProfile.phone : null);
    }
  }, [userProfile]);

  useEffect(() => {
    if (errorBackend2) setError('email', 'custom', errorBackend2);
  }, [errorBackend2]);

  // Get Therapist Per Availability
  useEffect(() => {
    let availability = {};
    if (!isEmpty(therapistPerAvailability.timeslots)) {
      therapistPerAvailability.timeslots.map((element) => {
        if (element.display_time == JSON.parse(router.query.info).time) {
          (availability['start'] = element.start),
            (availability['end'] = element.end);
        }
      });
      setAvailabilityData(availability);
    }
  }, [therapistPerAvailability]);

  const fetchUserData = async () => {
    let { status, data } = await dispatch(getUserProfileData(true));
    if (status) await dispatch(getCreditCards());
    await dispatch(getBookingGuests());
  };

  const fetchData = async (slug) => {
    if (!isEmpty(JSON.parse(router.query.info).dateTime)) {
      setDateTime(JSON.parse(router.query.info).dateTime);
    }
    // Get ProjectPricing
    let projectRes = await dispatch(getProjectPricingPerTherapist({ slug }));

    if (!isEmpty(projectRes) && projectRes.status) {
      if (!isEmpty(projectRes.data)) {
        setProjectPricing(
          projectRes.data.map((element) => {
            return {
              id: element.id,
              value: element.description,
            };
          })
        );
        setPricing(
          projectRes.data.map((element) => {
            return {
              id: element.id,
              price: element.pricing,
            };
          })
        );

        setProjectIdSpecific(Number(JSON.parse(router.query.info).projectId));
      }
    }

    // Get discount validity
    let discountRes = await dispatch(getValidDiscount({ slug }));
    if (!isEmpty(discountRes) && projectRes.status) {
      if (!isEmpty(discountRes.data)) {
        setDiscountValid(discountRes.data['discount-valid']);
      }
    }

    // Get Availability of Therapist
    let projectId = 0;
    if (JSON.parse(router.query.info).projectId) {
      dispatch(
        getTherapistAvailability({
          slug: router.query.order,
          projectId: JSON.parse(router.query.info).projectId,
          dateTime: JSON.parse(router.query.info).dateTime,
        })
      );
      let { data, status } = await dispatch(
        getTherapistData({
          slug: router.query.order,
          projectId: JSON.parse(router.query.info).projectId,
          dateTime: JSON.parse(router.query.info).dateTime,
        })
      );
      if (status && !isEmpty(data)) {
        const dataTherapist = Array.isArray(data?.profileSlotsData)
          ? data?.profileSlotsData?.[0]
          : data?.profileSlotsData;
        if (!isEmpty(dataTherapist)) {
          setSpecialistArr(
            dataTherapist.manager_specialities.map((element) => {
              return {
                id: element.code,
                value: element.description,
              };
            })
          );
          let defaultExist = false;
          dataTherapist.manager_specialities.map((element) => {
            if (element.default) {
              defaultExist = true;
              projectId = element.code;
              if (!router.query.massageType) {
                setTypeIdSpecific(element.code);
              } else {
                setTypeIdSpecific(router.query.massageType);
              }
            }
          });
          if (!defaultExist) {
            setTypeIdSpecific(dataTherapist.manager_specialities[0].code);
            projectId = dataTherapist.manager_specialities[0].code;
          }
        }
      }
    }
  };

  const handleProjectChange = (data) => {
    projectPricing.map((list) => {
      if (list.id === data.id) setProjectIdSpecific(data.id);
    });

    if (data.id !== projectIdSpecific) {
      dispatch(
        getTherapistAvailability({
          slug: router.query.order,
          projectId: data.id,
          dateTime: dateValue,
        })
      );
    }
  };

  const handleTypeChange = (data) => {
    specialistValue.map((list) => {
      if (list.id === data.id) setTypeIdSpecific(data.id);
    });
  };

  const convertDate = (date) => {
    if (!isEmpty(router.query)) {
      let months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
      ];
      let temp_date = date[0].split('-');
      return (
        temp_date[2] +
        ' ' +
        months[Number(temp_date[1]) - 1] +
        ', ' +
        temp_date[0]
      );
    }
  };

  const onSubmitCard = async (data) => {
    const cardElement = elements.getElement(CardElement);
    let payload = {
      stripe: stripe,
      cardElement: cardElement,
    };

    var { status } = await dispatch(addCard(payload));

    if (status) {
      if (forceCreditCardCreation) {
        window.dataLayer.push({
          event: 'CheckoutProcessAddingCC',
          event_category: 'clicks',
          data: null,
        });
        await handlePay();
      }
      if (!forceCreditCardCreation) {
        clearCardEdit();
      } else {
        setForceCreditCardCreation(false);
      }
    }
  };

  const handleCardEdit = () => {
    clearCardEdit();
    setForceCreditCardCreation(false);
  };

  const clearCardEdit = () => {
    const cardElement = elements.getElement(CardElement);
    cardElement?.clear();

    setCurrentPaymentType(currentCard);
  };

  const handlePayment = (data) => {
    creditCardData.map((list) => {
      if (list.id === data.id) setCurrentCard(data.id);
    });
  };

  const handlePaymentType = (data) => {
    setCurrentPaymentType(data.id);
  };

  const handleGuestChange = (data) => {
    setCurrentGuest(data.id);
    setDefaultName([data]);
    setBookingNameModalShow(false);
  };

  const handleClick = async () => {
    window.dataLayer.push({
      event: 'InitCheckoutProcess',
      event_category: 'clicks',
      data: null,
    });
    if (userProfile.phone && userProfile.email) {
      await handleBookingConfirmation();
    } else {
      setModalShow(true);
    }
  };

  const handleBookingConfirmation = async () => {
    if (!userProfile.require_credit_card) {
      await handlePay();
    } else {
      setForceCreditCardCreation(true);
    }
  };

  const handleNameChange = (e) => {
    setBookingName(e.target.value);
  };

  const handlePay = async () => {
    let payload = {
      paid_by: `${
        currentPaymentType == 'CA' || currentPaymentType == 'DB'
          ? currentPaymentType
          : 'CR'
      }`,
      app_source: 'web',
      start: `${
        availabilityData && availabilityData.start
          ? availabilityData.start
          : JSON.parse(router.query.info).start
      }`,
      end: `${
        availabilityData && availabilityData.end
          ? availabilityData.end
          : JSON.parse(router.query.info).end
      }`,
      userTimezone:
        Intl.DateTimeFormat()
          .resolvedOptions()
          .timeZone.replace(timezoneRegexFormat, '_') || 'America_Regina',
      card_id: `${
        currentPaymentType == 'CA' ||
        currentPaymentType == 'DB' ||
        isNaN(currentPaymentType)
          ? ''
          : currentPaymentType
      }`,
    };
    if (defaultName[0].id !== 0) {
      payload.userguest_id = defaultName[0].id;
    }
    if (!isEmpty(promoCodeData)) {
      payload['promo-codes'] = promoCodeData.code;
    }

    if (projectIdSpecific) {
      let { status, data } = await dispatch(
        bookMassageAppoinment({
          slug: router.query.order,
          projectId: projectIdSpecific,
          managerspecialityId: typeIdSpecific,
          payload,
        })
      );

      if (status) {
        await dispatch(getRewardData());
        setConfirmData(data.data);
        // setModalConfirmShow(true);
        router.push({
          pathname: `/booked`,
          query: {
            confirmData: data?.data?.booking?.booking_slug,
          },
        });

        const durationValuePayload = projectPricing.find(
          (element) => element.id === projectIdSpecific
        );

        /* FBP Detail Cart */
        // window.fbq('trackCustom', 'checkout_comeplete', {
        //   actionItem: 'booked',
        //   data: {
        //     id: router.query.specialist,
        //     list_name: therapistIndividual.city,
        //     massage_date: data.start,
        //     duration: durationValuePayload.description,
        //   },
        //   event_label: router.query.specialist,
        //   event_category: 'cart',
        // });

        /* Gtag Detail Cart */
        window.dataLayer.push({
          event: 'checkoutCart',
          event_label: therapistIndividual.slug,
          event_category: 'cart',
          actionItem: 'booked',
          data: null,
        });

        //router.push(`/consent-form?url=${data.data['covid-form']}`);
        // router.push(
        //   `/order-detail/[detail]?info=${JSON.stringify({ confirm: true })}`,
        //   `/order-detail/${
        //     data.data.booking.booking_slug
        //   }?info=${JSON.stringify({
        //     confirm: true,
        //   })}`
        // );
      }
    }
  };

  // FIXME - Need to confirm
  const updateEmailPhone = async (data) => {
    var emailStatus = true;
    var phoneStatus = true;

    if (!userProfile.email && data.email) {
      let { status } = await dispatch(userEmailUpdate({ email: data.email }));
      emailStatus = status;
    }
    if (emailStatus && !userProfile.phone && data.phone) {
      data.lastname = userProfile.lastname;
      data.firstname = userProfile.firstname;
      let { status } = await dispatch(profileUpdate(data));
      phoneStatus = status;
    }
    setModalShow(false);
    if (emailStatus && phoneStatus) {
      window.dataLayer.push({
        event: 'CheckoutProcessAddingClientContactInfo',
        event_category: 'clicks',
        data: null,
      });
      handleBookingConfirmation();
    }
  };

  const getTotalAmount = (pricing) => {
    let totalReward = 0;
    let totalTax = 0;

    if (!isEmpty(promoCodeData)) {
      totalReward = parseFloat(promoCodeData.reward);
    }
    if (isEmpty(promoCodeData) && userRewards?.reward) {
      totalReward = parseFloat(userRewards.reward);
    }

    if (pricing.taxes) {
      totalTax = parseFloat(pricing.taxes);
    }

    return `${(parseFloat(pricing.amount) - totalReward + totalTax).toFixed(
      2
    )}`;
  };

  const handleBackClick = () => {
    if (!isEmpty(therapistIndividual)) {
      // const durationValuePayload = projectPricing.find(
      //   (element) => element.id === projectIdSpecific
      // );

      /* FBP Order Remove Cart */
      // window.fbq('trackCustom', 'remove_from_cart', {
      //   actionItem: 'cancel',
      //   data: {
      //     id: router.query.order,
      //     list_name: therapistIndividual.city,
      //     massage_date: JSON.parse(router.query.info).dateTime.toString(),
      //     duration: durationValuePayload.description,
      //   },
      //   event_label: router.query.specialist,
      //   event_category: 'cart',
      // });

      /* Gtag Order Remove Cart */
      window.dataLayer.push({
        event: 'removedFromCart',
        event_label: therapistIndividual.slug,
        event_category: 'cart',
        actionItem: 'cancel',
        data: null,
      });
    }

    router.back();
  };

  const handleChangePromoCode = (e) => {
    setPromoCodeValue(e.target.value);
  };

  const handleCanclePromocode = () => {
    setPromoCodeData({});
    projectPrice.map((data) => {
      if (data.id == projectIdSpecific) {
        return getTotalAmount(data.price);
      }
    });
  };

  const handleSendPromoCode = async () => {
    if (isLoggedIn) {
      let { status, data } = await dispatch(
        getPromoCode({ code: promoCodeValue })
      );
      if (status) {
        setPromoCodeData(data?.data);
        setIsNotLoginError(false);
        setPromoCodeValue('');
      }
    } else {
      setIsNotLoginError(true);
      setPromoCodeValue('');
      setTimeout(() => {
        setIsNotLoginError(false);
      }, 3000);
    }
  };
  const handleRedirect = async (type) => {
    const query = new URLSearchParams(
      confirmData?.['intake-form']?.intakeFormAPIUrl
    );
    type == 'intake'
      ? router.push({
          pathname: `/intake-form/${query.get('form')}`,
          query: {
            type: 'Active',
            intake: confirmData?.['intake-form']?.intakeFormAPIUrl,
            covid: JSON.stringify(confirmData?.['covid-form']),
            booking: confirmData.booking.booking_slug,
            book_data: JSON.stringify({
              therapist_name: confirmData.booking.therapist_name,
              guest: confirmData.booking.guest,
            }),
          },
        })
      : router.push({
          pathname: `/consent-form`,
          query: {
            covid: JSON.stringify(confirmData?.['covid-form']),
            booking: confirmData.booking.booking_slug,
            book_data: JSON.stringify({
              therapist_name: confirmData.booking.therapist_name,
              guest: confirmData.booking.guest,
            }),
          },
        });
  };

  const handleRedirectNewIntake = async () => {
    router.push({
      pathname: `/intake-form/new`,
      query: {
        type: 'new',
        intake: confirmData?.['intake-form']?.intakeFormAPIUrl,
        covid: JSON.stringify(confirmData?.['covid-form']),
        booking: confirmData.booking.booking_slug,
        book_data: JSON.stringify({
          therapist_name: confirmData.booking.therapist_name,
          guest: confirmData.booking.guest,
        }),
      },
    });
  };

  const handleRedirectOrder = async () => {
    let payload = {
      url: confirmData['intake-form']?.intakeFormAPIUrl,
      data: {
        modifier: 'C',
      },
    };
    const { status } = await dispatch(consentIntakeForm(payload));
    if (status) {
      router.push({
        pathname: '/consent-form',
        query: {
          covid: JSON.stringify(confirmData['covid-form']),
          booking: confirmData.booking.booking_slug,
          book_data: JSON.stringify({
            therapist_name: confirmData.booking.therapist_name,
            guest: confirmData.booking.guest,
          }),
        },
      });
    }
  };

  const handleRedirectIntakeUpdate = async () => {
    const query = new URLSearchParams(
      confirmData?.['intake-form']?.intakeFormAPIUrl
    );
    router.push({
      pathname: `/intake-form/${query.get('form')}`,
      query: {
        intake: confirmData?.['intake-form']?.intakeFormAPIUrl,
        covid: JSON.stringify(confirmData?.['covid-form']),
        booking: confirmData.booking.booking_slug,
        book_data: JSON.stringify({
          therapist_name: confirmData.booking.therapist_name,
          guest: confirmData.booking.guest,
        }),
      },
    });
  };

  const handleGuest = async () => {
    let { status, data } = await dispatch(
      createBookingGuest({ fullname: bookingName })
    );
    if (status) {
      await dispatch(getBookingGuests());
      let therapist = {};
      therapist.id = data.data[data.data.length - 1].id || 0;
      therapist.value = bookingName;
      setDefaultName([therapist]);
      setCurrentGuest(data.data[data.data.length - 1].id || 0);
      setBookingNameModalShow(false);
    }
  };

  const handlePromoCodeForm = () => {
    setShowPromoCodeForm(!showPromoCodeForm);
  };

  function CustomToggle({ eventKey }) {
    const handleOnClick = useAccordionToggle(eventKey, () => {
      setOpen(!open);
    });
    const [open, setOpen] = useState(false);
    return (
      <button
        type="button"
        className="w-100 d-flex align-items-center pl-0"
        style={{ border: 'unset', backgroundColor: 'unset' }}
        onClick={handleOnClick}
      >
        <h4 className="sub-title-local order-title d-inline float-left mb-0">
          Price Breakdown
        </h4>

        <i
          className={classNames({
            'text-red float-left ml-2': true,
            'fas fa-angle-right': !open,
            'fas fa-angle-down': open,
          })}
        ></i>
      </button>
    );
  }

  return (
    <PageLoader visible={loaderCount <= 0 ? false : true}>
      <div className="content-wrapper">
        <Head>
          <title>Place Order - Thrivr</title>
        </Head>

        {/* Header Section */}
        <Header className="header-white" />

        {/* Banner Section */}
        <OrderStyleWrapper>
          <Container>
            {errorBackend2 && typeof errorBackend2 === 'string' && (
              <ErrorAlert message={errorBackend2} />
            )}
            {/* Error Alert */}
            {/* Alert Error Message */}
            <AnimatePresence>
              {errorBackend ? (
                <motion.div
                  className="alert-wrapper"
                  initial={{ opacity: 0 }}
                  animate={{ opacity: 1 }}
                  transition={{ duration: 0.3 }}
                  exit={{ opacity: 0 }}
                >
                  <Alert variant={'error'}>{errorBackend}</Alert>
                </motion.div>
              ) : (
                loginError && (
                  <motion.div
                    className="alert-wrapper"
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }}
                    transition={{ duration: 0.3 }}
                    exit={{ opacity: 0 }}
                  >
                    <Alert variant={'error'}>
                      You can apply Promo/Gift Card after logging in
                    </Alert>
                  </motion.div>
                )
              )}
            </AnimatePresence>
            <div className="order-confirm-wrapper">
              <a onClick={() => handleBackClick()} className="back-arrow">
                <img width="28" height="28" src="/images/left.svg" />
              </a>
              <div className="order-header">
                <CustomModal
                  show={modalConfirmShow}
                  size="lg"
                  aria-labelledby="contained-modal-title-vcenter"
                  centered
                  backdrop="static"
                  className="modal-cancle desktop-alt massage-confirm consent-popup"
                  header={
                    <>
                      <h5
                        className="modal-title d-sm-block"
                        id="exampleModalLabel"
                      >
                        Massage Confirmed
                      </h5>
                    </>
                  }
                  body={
                    <div className="confirm-content">
                      {confirmData?.['intake-form']?.alreadyConsented
                        ? 'Would you like to update your intake form?'
                        : confirmData?.['intake-form']?.intakeForm
                        ? `${'Do you consent to share your intake form with '}${
                            therapistIndividual.manager_first_name
                          } ${therapistIndividual.manager_last_name}?`
                        : 'Please fill the intake form before your appointment.'}
                    </div>
                  }
                  footer={
                    <React.Fragment>
                      {confirmData?.['intake-form']?.alreadyConsented ? (
                        <>
                          <Button
                            className="btn d-sm-block"
                            onClick={() => handleRedirect('covid')}
                          >
                            Next
                          </Button>
                          <Button
                            className="btn d-sm-block"
                            onClick={() => handleRedirect('intake')}
                          >
                            Update
                          </Button>
                        </>
                      ) : confirmData?.['intake-form']?.intakeForm ? (
                        <>
                          <Button
                            className="btn d-sm-block"
                            onClick={handleRedirectOrder}
                          >
                            I consent
                          </Button>
                          <Button
                            className="btn d-sm-block"
                            onClick={handleRedirectIntakeUpdate}
                          >
                            Update
                          </Button>
                        </>
                      ) : (
                        <Button
                          className="btn d-sm-block"
                          onClick={handleRedirectNewIntake}
                        >
                          Fill Form
                        </Button>
                      )}
                    </React.Fragment>
                  }
                />

                <div className="massage-date-time">
                  <h4 className="sub-title-local order-title">
                    Step 1. Confirm Details
                  </h4>
                  <h3 className="order-sub-title">
                    {!isEmpty(router?.query?.info)
                      ? !isEmpty(JSON.parse(router.query.info).dateTime)
                        ? convertDate(
                            JSON.parse(router.query.info).dateTime.split(' ')
                          )
                        : '-'
                      : '-'}
                  </h3>
                  <h3 className="order-sub-title">
                    {!isEmpty(router?.query?.info)
                      ? !isEmpty(JSON.parse(router.query.info).time)
                        ? JSON.parse(router.query.info).time
                        : '-'
                      : '-'}{' '}
                    (
                    {
                      projectPricing.find((e) => e.id == projectIdSpecific)
                        ?.value
                    }
                    )
                  </h3>
                </div>

                {/* <div className="massage-duration">
                  <h4 className="sub-title order-title">Duration</h4>
                  <div className="minutes-dropdown">
                    <InputGroup isCustom className="input-group">
                      <CustomDropdown
                        list={projectPricing}
                        value={projectIdSpecific}
                        listClassname="hours"
                        placeholderClassname="profile-title"
                        placeholder="Min"
                        handleDropdownChange={handleProjectChange}
                      />
                    </InputGroup>
                  </div>
                </div> */}

                <CustomModal
                  show={bookingNameModalShow}
                  onHide={() => setBookingNameModalShow(false)}
                  size="lg"
                  className="modal-cancle desktop-alt"
                  container={() =>
                    document.querySelector('.order-confirm-wrapper')
                  }
                  header={
                    <h5 className="modal-title" id="exampleModalLabel">
                      Enter your name here..
                    </h5>
                  }
                  body={
                    <>
                      <div className="pay-option">
                        <div className="payment-option">
                          <div className="input-group">
                            <CustomDropdown
                              list={guestData}
                              value={currentGuest}
                              className="custom-dd guest-dd"
                              listClassname="payby"
                              placeholder="Select User"
                              handleDropdownChange={handleGuestChange}
                            />
                          </div>
                        </div>
                      </div>
                      <InputGroup isCustom>
                        <Input
                          type="text"
                          placeholder="Enter full name"
                          name="name"
                          onChange={handleNameChange}
                        />
                      </InputGroup>
                    </>
                  }
                  footer={
                    !isEmpty(userProfile) ? (
                      <Button
                        className={`${
                          bookingName ? '' : 'disabled'
                        } ${'btn mobile-btn'}`}
                        onClick={handleGuest}
                      >
                        Save
                      </Button>
                    ) : (
                      <Button
                        className="login"
                        href={`/login?redirectFrom=${router?.asPath}`}
                      >
                        Log in to continue
                      </Button>
                    )
                  }
                />
              </div>
              <div className="order-confirm-name">
                <div className="main-img">
                  {!isEmpty(therapistIndividual) ? (
                    therapistIndividual.profile_photo ? (
                      <img
                        alt="RMT avatar"
                        src={therapistIndividual.profile_photo}
                      />
                    ) : (
                      <img alt="RMT avatar" src="/images/avatar.png" />
                    )
                  ) : (
                    <img alt="RMT avatar" src="/images/avatar.png" />
                  )}
                </div>
                <div className="specialist-info">
                  <h4 className="sub-title">
                    {!isEmpty(therapistIndividual)
                      ? `${therapistIndividual.manager_first_name} ${therapistIndividual.manager_last_name}`
                      : '-'}
                  </h4>
                  <div className="location">
                    {!isEmpty(therapistIndividual)
                      ? !isEmpty(therapistIndividual.address)
                        ? therapistIndividual.address
                        : '-'
                      : '-'}
                    {' - '}
                    {!isEmpty(therapistIndividual?.address_description)
                      ? therapistIndividual?.address_description
                      : ''}
                  </div>
                  {!isEmpty(therapistIndividual.parking_description) && (
                    <div>
                      {therapistIndividual.parking ? 'Free ' : ''}
                      Parking
                      {therapistIndividual.parking_description}
                    </div>
                  )}
                </div>
              </div>
              <div className="text-center mt-1 pl-2 pr-2">
                <a
                  varient="secondary"
                  onClick={() => setBookingNameModalShow(true)}
                  className="text-red"
                >
                  <u>
                    Booking for{' '}
                    {currentGuest
                      ? !isEmpty(defaultName)
                        ? defaultName[0].value
                        : 'someone else?'
                      : 'someone else?'}
                  </u>
                </a>
              </div>

              <SeperatorStyle className="seperator" />
              <div className="massage-type">
                <h4 className="sub-title-local order-title">
                  Step 2. Therapy Type
                </h4>
                <div className="minutes-dropdown">
                  <InputGroup isCustom className="input-group">
                    <CustomDropdown
                      list={specialistValue}
                      value={typeIdSpecific}
                      listClassname="hours"
                      placeholderClassname="profile-title"
                      placeholder="Type"
                      handleDropdownChange={handleTypeChange}
                    />
                  </InputGroup>
                </div>
              </div>
              <SeperatorStyle className="seperator" />
              <div className="price-breakdown">
                {/* {isLoggedIn && userRewards?.reward && (
                  <div className="payment-details">
                    <div className="discount-banner">
                      <CustomChipBanner
                        children={
                          <span className="text-center">
                            {userRewards?.reward_text}
                          </span>
                        }
                      />
                    </div>
                  </div>
                )} */}
                <Accordion>
                  <Card style={{ border: 'unset' }}>
                    <Card.Header
                      className="pl-0"
                      style={{
                        backgroundColor: 'unset',
                        border: 'unset',
                      }}
                    >
                      <CustomToggle eventKey="0"></CustomToggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                      <Card.Body className="pr-0 pl-0">
                        <div className="payment-details">
                          <h4 className="sub-title price-label">Price</h4>
                          <h4 className="sub-title price float-right">
                            {!isEmpty(projectPrice)
                              ? projectPrice.map((data) => {
                                  if (data.id == projectIdSpecific) {
                                    return `${'CAD $'}${data.price.amount}`;
                                  }
                                })
                              : '-'}
                          </h4>
                        </div>
                        <div className="payment-details">
                          <h4 className="sub-title price-label">GST</h4>
                          <h4 className="sub-title price float-right">
                            {!isEmpty(projectPrice)
                              ? projectPrice.map((data) => {
                                  if (data.id == projectIdSpecific) {
                                    return `${'CAD $'}${data.price.taxes}`;
                                  }
                                })
                              : ''}
                          </h4>
                        </div>
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                </Accordion>

                {isLoggedIn && !isEmpty(promoCodeData) ? (
                  <div className="payment-details discounts">
                    <h4 className="sub-title price-label">Discount</h4>
                    <h4 className="sub-title price float-right">
                      {`${'CAD $'}${promoCodeData?.reward?.toFixed(2)}`}
                      {'   '}&nbsp;
                      <span className="signup-opt w-100">
                        <a onClick={handleCanclePromocode}>X</a>
                      </span>
                    </h4>
                  </div>
                ) : showPromoCodeForm ? (
                  <div className="payment-details promo-code mt-3">
                    <InputGroup isCustom>
                      <Input
                        name="email"
                        className="payment-details promo-code input-stl"
                        placeholder={
                          !isLoggedIn
                            ? 'Login to apply promo code'
                            : 'promo code'
                        }
                        value={promoCodeValue}
                        onChange={handleChangePromoCode}
                        disabled={!isLoggedIn ? true : false}
                      />
                    </InputGroup>
                    <a
                      onClick={() => setShowPromoCodeForm(!showPromoCodeForm)}
                      className="action-btn text-red align-middle mb-1 d-flex align-items-center mr-3 "
                      style={{ textDecoration: 'none !important' }}
                    >
                      {' '}
                      Cancel{' '}
                    </a>
                    <Button
                      disabled={isEmpty(promoCodeValue) ? true : false}
                      onClick={handleSendPromoCode}
                      className="payment-details promo-code button-stl mr-0"
                    >
                      Apply
                    </Button>
                  </div>
                ) : (
                  <div className="text-red mt-3">
                    {' '}
                    <a onClick={handlePromoCodeForm}>Apply Promocode</a>{' '}
                  </div>
                )}
                {userRewards?.reward && isEmpty(promoCodeData) && (
                  <div className="payment-details">
                    <h4 className="sub-title price-label">Discount</h4>
                    <h4 className="sub-title price">{`${'CAD $'}${
                      userRewards?.reward
                    }`}</h4>
                  </div>
                )}
                <div className="payment-details total-amount">
                  <h3 className="order-sub-title price-label">Total amount</h3>
                  <h3 className="order-sub-title price ">
                    {!isEmpty(projectPrice)
                      ? projectPrice.map((data) => {
                          if (data.id == projectIdSpecific) {
                            return `${'CAD $'}${getTotalAmount(data.price)}`;
                          }
                        })
                      : ''}
                  </h3>
                </div>

                <div className="login-option text-center mt-0">
                  <span className="signup-opt w-100">
                    <a onClick={() => setCancelModalShow(true)}>
                      Cancellation policy
                    </a>
                  </span>
                </div>
                <CustomModal
                  show={cancelModalShow}
                  onHide={() => setCancelModalShow(false)}
                  container={() => document.querySelector('.login-option')}
                  size="lg"
                  className="modal-cancle cancellation-popup desktop-alt"
                  header={
                    <>
                      <h4 className="modal-title d-sm-block">
                        Cancellation Policy
                      </h4>
                    </>
                  }
                  body={
                    <ul>
                      <li className="modal-list p-2 text-left">
                        You may cancel or reschedule your booking up to 1 hour
                        before your appointment with no penalty.
                      </li>
                      <li className="modal-list p-2 text-left">
                        If you reschedule with less than 1 hours' notice, you
                        will be charged 25% of your appointment fee.
                      </li>
                      <li className="modal-list p-2 text-left">
                        If you cancel with less than 1 hours' notice, you will
                        be charged a 50% of your appointment fee.
                      </li>
                      <li className="modal-list p-2 text-left">
                        If a therapist is not available for your booking, you
                        will not be charged.
                      </li>
                    </ul>
                  }
                  footer={
                    <Button
                      className="btn mobile-btn"
                      onClick={() => setCancelModalShow(false)}
                    >
                      Close
                    </Button>
                  }
                />
              </div>
              <SeperatorStyle className="seperator" />

              {!isEmpty(userProfile) ? (
                <div className="pay-option">
                  <h4 className="sub-title-local order-title">
                    Step 3. Pay Options
                  </h4>
                  <div className="payment-mode">
                    <div className="pay-check">
                      <div className="payment-option">
                        <div className="input-group">
                          <CustomDropdown
                            list={creditCardData}
                            value={currentPaymentType}
                            className="custom-dd"
                            listClassname="payby"
                            placeholder="Choose method"
                            handleDropdownChange={handlePaymentType}
                            type="payment"
                          />
                        </div>
                      </div>
                    </div>

                    {currentPaymentType == 'addnew' ? (
                      <div className="new-payment-method">
                        <StripeCreditCardInput
                          show={currentPaymentType}
                          handleOnCreate={(data) => {
                            onSubmitCard(data);
                          }}
                          handleOnClose={handleCardEdit}
                          setCancelModalShow={setCancelModalShow}
                          showCancelationPolicy={false}
                        ></StripeCreditCardInput>
                      </div>
                    ) : (
                      ''
                    )}
                  </div>
                </div>
              ) : (
                ''
              )}
              <div className="login-option text-center d-flex flex-column align-items-center">
                {!isEmpty(userProfile) ? (
                  currentPaymentType == 'CA' ||
                  currentPaymentType == 'DB' ||
                  (!isNaN(currentPaymentType) &&
                    currentPaymentType !== null) ? (
                    <Button
                      className="login button-content mb-3"
                      onClick={handleClick}
                    >
                      Confirm Booking
                    </Button>
                  ) : (
                    <Button className="btn disabled button-content mb-3">
                      Confirm Booking
                    </Button>
                  )
                ) : (
                  <Button
                    className="login button-content mb-3"
                    href={`/login?redirectFrom=${router?.asPath}${
                      typeIdSpecific ? '&massageType=' + typeIdSpecific : null
                    }`}
                  >
                    Log in to continue
                  </Button>
                )}
                <span className="button-subtext  text-red">
                  Payment is processed after the appointment
                </span>
                {!isEmpty(userProfile) ? (
                  ''
                ) : (
                  <span className="signup-opt w-100">
                    Don&apos;t have an account?{' '}
                    <a href={`/signup?redirectFrom=${router?.asPath}`}>
                      Sign up
                    </a>{' '}
                    <p>It's FREE</p>
                  </span>
                )}
                <CustomModal
                  show={modalShow}
                  onHide={() => setModalShow(false)}
                  size="lg"
                  className="modal-cancle desktop-alt"
                  header={
                    <h5 className="modal-title" id="exampleModalLabel">
                      To proceed please provide
                    </h5>
                  }
                  body={
                    <>
                      {userProfile.email !== null ? null : (
                        <InputGroup
                          className="pd-bottom-input-g"
                          isCustom={true}
                        >
                          <Input
                            type="text"
                            placeholder={
                              !isEmpty(userProfile) && userProfile.email
                                ? userProfile.email
                                : 'Email'
                            }
                            name="email"
                            refProps={register}
                            isInvalid={
                              !isEmpty(errorBackend2)
                                ? errorBackend2
                                : !isEmpty(errors) && !isEmpty(errors.email)
                            }
                          />
                          {!isEmpty(errors) && errors.email && (
                            <Form.Control.Feedback type="invalid">
                              {errors.email.message}
                            </Form.Control.Feedback>
                          )}
                        </InputGroup>
                      )}

                      {!isEmpty(userProfile.phone) ? null : (
                        <InputGroup isCustom={true}>
                          <Controller
                            mask="+1 (999) 999-9999"
                            className="form-control"
                            placeholder={'Your phone number'}
                            as={
                              <InputMask>
                                {(maskProps) => (
                                  <Input
                                    {...maskProps}
                                    isInvalid={
                                      !isEmpty(errors) && !isEmpty(errors.phone)
                                    }
                                    refProps={register}
                                  />
                                )}
                              </InputMask>
                            }
                            control={control}
                            name="phone"
                          />
                          {!isEmpty(errors) && errors.phone && (
                            <Form.Control.Feedback type="invalid">
                              {errors.phone.message}
                            </Form.Control.Feedback>
                          )}
                        </InputGroup>
                      )}
                    </>
                  }
                  footer={
                    <Button
                      className="btn mobile-btn"
                      onClick={emailPhoneHandleSubmit(updateEmailPhone)}
                    >
                      {userProfile.require_credit_card
                        ? 'Save'
                        : 'Confirm Booking'}
                    </Button>
                  }
                />
              </div>
              {forceCreditCardCreation ? (
                <div className="new-payment-method">
                  <StripeCreditCardInput
                    show={forceCreditCardCreation}
                    handleOnCreate={onSubmitCard}
                    handleOnClose={handleCardEdit}
                    showCancelationPolicyModal={setCancelModalShow}
                    showCancelationPolicyText={true}
                  ></StripeCreditCardInput>
                </div>
              ) : null}
            </div>
          </Container>
        </OrderStyleWrapper>

        {/* Footer Section */}
        <Footer />
      </div>
    </PageLoader>
  );
}

const PaymentTypeList = [
  {
    id: 'addnew',
    value: 'Add New Card',
  },
  {
    id: 'CA',
    value: 'Pay in Person',
  },
];

const PaymentTypeListWithDB = [
  {
    id: 'addnew',
    value: 'Add New Card',
  },
  {
    id: 'CA',
    value: 'Pay in Person',
  },
  {
    id: 'DB',
    value: 'Direct Billing',
  },
];
