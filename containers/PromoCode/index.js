import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import Container from 'react-bootstrap/Container';
import PageLoader from 'components/Views/PageLoader';
import ErrorAlert from 'components/ErrorAlert';
import isEmpty from 'lodash.isempty';
import Button from 'components/Base/Button';
import Footer from 'components/Footer';
import Header from 'components/Header';
import { IntakeFormStyleWrapper, OrderStyleWrapper } from './order.style';
import { SeperatorStyle } from 'components/StyleComponent';
import { LayoutDiv } from 'components/StyleComponent';
import Input from 'components/Base/Input';
import InputGroup from 'components/Base/InputGroup';
import { useForm } from 'react-hook-form';
import Form from 'react-bootstrap/Form';
import * as yup from 'yup';
import { inviteUser } from 'redux/app/actions';
import SuccessAlert from 'components/SuccessAlert';

const InitialFormData = {
  email: '',
  name: '',
};
const schema = yup.object().shape({
  email: yup.string().email().required(),
  name: yup.string().required(),
});

export default function PromoCode() {
  const loaderCount = useSelector((state) => state.app.loaderCount);
  const userProfile = useSelector((state) => state.auth.userProfile);
  const [inviteSuccess, setInviteSuccess] = useState('');
  const [copied, setCopied] = useState(false);
  const [isMobile, setIsmobile] = useState(false);
  const [referralLink, setReferralLink] = useState(null);
  const { register, handleSubmit, errors, setError } = useForm({
    validationSchema: schema,
    defaultValues: {
      ...InitialFormData,
    },
  });

  const errorBackend = useSelector((state) => state.auth.error);
  const dispatch = useDispatch();
  const router = useRouter();

  const handleCopy = () => {
    navigator.clipboard.writeText(
      window.location.origin + '/signup?referral=' + userProfile?.referral_code
    );
    setCopied(true);
  };

  useEffect(() => {
    setReferralLink(
      `${process.env.WEBAPP_URL}/signup?referral=${userProfile?.referral_code}`
    );
  });

  useEffect(() => {
    let hasTouchScreen = false;
    if ('maxTouchPoints' in navigator) {
      hasTouchScreen = navigator.maxTouchPoints > 0;
    } else if ('msMaxTouchPoints' in navigator) {
      hasTouchScreen = navigator.msMaxTouchPoints > 0;
    } else {
      const mQ = window.matchMedia && matchMedia('(pointer:coarse)');
      if (mQ && mQ.media === '(pointer:coarse)') {
        hasTouchScreen = !!mQ.matches;
      } else if ('orientation' in window) {
        hasTouchScreen = true;
      } else {
        var UA = navigator.userAgent;
        hasTouchScreen =
          /\b(BlackBerry|webOS|iPhone|IEMobile)\b/i.test(UA) ||
          /\b(Android|Windows Phone|iPad|iPod)\b/i.test(UA);
      }
    }
    if (hasTouchScreen) {
      setIsmobile(true);
    } else {
      setIsmobile(false);
    }
  }, []);

  const urlEncodeMessage = () => {
    var message = getReferalLinkMessage();
    return encodeURI(message);
  };

  const getReferalLinkMessage = () => {
    return `Your friend ${userProfile.firstname}, thought you could use a little rest and relaxation so they sent you $10 off toward a relaxing massage with Thrivr! Use this link to `;
  };
  const onSubmit = async (data) => {
    let payload = {
      ...data,
    };
    const { status, message } = await dispatch(inviteUser(payload));

    if (status) {
      setInviteSuccess(message);
    } else {
      setError('email', 'custom', message);
    }
    setTimeout(() => {
      setInviteSuccess('');
    }, 4000);
  };

  return (
    <PageLoader visible={loaderCount <= 0 ? false : true}>
      <div className="content-wrapper">
        <Head>
          <title>Promo Code - Thrivr</title>
        </Head>

        {/* Header Section */}
        <Header className="header-white" />

        {/* Banner Section */}
        <LayoutDiv
          style={{
            minHeight: '0px',
          }}
        >
          <IntakeFormStyleWrapper
            toppadding={router.pathname === '/booked' ? 120 : 0}
            minHeight={router.pathname === '/booked' ? true : false}
          >
            <OrderStyleWrapper>
              <Container>
                {/* Error Alert */}
                {errorBackend && typeof errorBackend === 'string' && (
                  <ErrorAlert message={errorBackend} />
                )}

                {inviteSuccess && <SuccessAlert message={inviteSuccess} />}

                <div className="order-confirm-wrapper">
                  <div className="order-header">
                    <h4 className="code-title"> Your Code</h4>
                    <div className="promo-code-styling text-center">
                      <p className="promo-code-name-st">
                        {userProfile?.referral_code}
                      </p>

                      <Button className=" btn-sm mb-1" onClick={handleCopy}>
                        {copied ? (
                          'Copied!'
                        ) : (
                          <React.Fragment>
                            {' '}
                            Copy to clipboard{' '}
                            <i className="far fa-copy ml-1"></i>
                          </React.Fragment>
                        )}
                      </Button>
                    </div>
                    <p>
                      Share your code with a friend. When they book a massage
                      you get $10 off your next and they get $10 of their
                      massage.
                    </p>
                    <div
                      className="col-12 d-flex align-items-center justify-content-center"
                      style={{ fontSize: '30px' }}
                    >
                      {isMobile ? (
                        <a
                          href={`sms:?&body=${urlEncodeMessage()} ${referralLink}`}
                          style={{ color: '#3ED490' }}
                          className="share-link"
                        >
                          <i className="fas fa-sms m-2 share-icon"></i>
                          <p className="share-link-heading">Text</p>
                        </a>
                      ) : null}

                      <a
                        href={`mailto:?subject=${encodeURI(
                          `$10 off from ${userProfile.firstname}`
                        )}&body=${urlEncodeMessage()} ${
                          process.env.WEBAPP_URL
                        }/signup%3Freferral%3D${userProfile?.referral_code}`}
                        className="share-link"
                        style={{ color: '#3ED490' }}
                      >
                        <i className="fas fa-envelope-open-text m-2"></i>
                        <p className="share-link-heading">Email</p>
                      </a>
                      {isMobile ? (
                        <a
                          href={`fb-messenger://share/?link=${encodeURI(
                            referralLink
                          )}`}
                          style={{ color: '#3ED490' }}
                          className="share-link"
                        >
                          <i className="fab fa-facebook-messenger m-2 share-icon"></i>
                          <p className="share-link-heading">Messenger</p>
                        </a>
                      ) : (
                        <a
                          href={`https://www.facebook.com/sharer/sharer.php?u=${encodeURI(
                            referralLink
                          )};src=sdkpreparse`}
                          target="_blank"
                          className="share-link"
                          style={{ color: '#3ED490' }}
                        >
                          {' '}
                          <i className="fab fa-facebook-square  m-2 share-icon"></i>
                          <p
                            className="share-link-heading"
                            style={{ fontSize: '14px' }}
                          >
                            Facebook
                          </p>
                        </a>
                      )}
                    </div>
                    <div className="text-center">
                      <p>
                        {' '}
                        <b>Click on a icon to share</b>
                      </p>
                    </div>

                    <hr className="mt-3 mb-3 separator-hr" />

                    <h4 className="code-title"> Invite User</h4>

                    <Form onSubmit={handleSubmit(onSubmit)}>
                      <InputGroup className="bottom-styl-input" isCustom>
                        <Input
                          name="name"
                          placeholder="Name"
                          refProps={register}
                          isInvalid={!isEmpty(errors) && !isEmpty(errors.name)}
                        />
                        {!isEmpty(errors) && errors.name && (
                          <Form.Control.Feedback type="invalid">
                            {errors.name.message}
                          </Form.Control.Feedback>
                        )}
                      </InputGroup>
                      <InputGroup isCustom>
                        <Input
                          name="email"
                          placeholder="Email"
                          refProps={register}
                          isInvalid={!isEmpty(errors) && !isEmpty(errors.email)}
                        />
                        {!isEmpty(errors) && errors.email && (
                          <Form.Control.Feedback type="invalid">
                            {errors.email.message}
                          </Form.Control.Feedback>
                        )}
                      </InputGroup>
                      <Button className="btn-promocode" type="submit">
                        Invite
                      </Button>
                    </Form>
                  </div>
                </div>
              </Container>
            </OrderStyleWrapper>
          </IntakeFormStyleWrapper>
        </LayoutDiv>

        {/* Footer Section */}
        <Footer />
      </div>
    </PageLoader>
  );
}
