import styled from 'styled-components';

const StyledMap = styled.div`
  .clickable {
    cursor: pointer;
  }

  .underline {
    color: rgb(56, 56, 56);
    text-decoration: underline;
    -webkit-text-decoration-color: ${(props) =>
      props.theme.color.pink}; /* safari still uses vendor prefix */
    text-decoration-color: ${(props) => props.theme.color.pink};
    text-decoration-style: dashed;
    -webkit-text-decoration-style: dashed;
    text-decoration-thickness: 2px;
    -webkit-text-decoration-thickness: 2px;
  }
`;

export default StyledMap;
