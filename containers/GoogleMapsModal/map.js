import React from 'react';
import GoogleMapReact from 'google-map-react';
import StyledMarker from './marker.style';

const showInMapClicked = (lat, lng) => {
  window.open('https://maps.google.com?q=' + lat + ',' + lng);
};

export default function Map(props) {
  if (props.lat && props.lng) {
    const defaultProps = {
      center: {
        lat: props.lat,
        lng: props.lng,
      },
      zoom: 11,
      options: {
        fullscreenControl: false,
      },
    };

    return (
      // Important! Always set the container height explicitly
      <div style={{ height: '270px', width: '270px' }}>
        <GoogleMapReact
          bootstrapURLKeys={{
            key: process.env.ADDRESS_AUTOCOMPLETE_API_KEY,
            libraries: ['places'],
          }}
          defaultCenter={defaultProps.center}
          defaultZoom={defaultProps.zoom}
          options={defaultProps.options}
          disableDefaultUI
        >
          <Marker lat={props.lat} lng={props.lng} />
        </GoogleMapReact>
      </div>
    );
  } else {
    return <div>Address not found.</div>;
  }
}

const Marker = (props) => {
  return (
    <>
      <StyledMarker>
        <div
          className="pin"
          onClick={() => showInMapClicked(props.lat, props.lng)}
        ></div>
        <div className="pulse"></div>
      </StyledMarker>
    </>
  );
};
