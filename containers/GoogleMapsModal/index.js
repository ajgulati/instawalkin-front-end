import Map from './map';
import PopoverStickOnHover from './PopoverStickOnHover';
import StyledMap from './googlemapsmodal.style';
import React from 'react';
import { OverlayTrigger } from 'react-bootstrap';

export default function GoogleMapsModal(props) {
  if ('ontouchstart' in window) {
    return (
      <StyledMap>
        <div>
          <OverlayTrigger
            rootClose={true}
            onHide={() => this.setState({ show: false })}
            trigger={['click']}
            placement="bottom"
            overlay={
              <div>
                <Map lat={props.lat} lng={props.lng} />
              </div>
            }
          >
            <a className="clickable underline">
              <img
                width="15"
                height="15"
                alt="rating start"
                src="/images/location.svg"
              />
              &nbsp;
              {props.address}
            </a>
          </OverlayTrigger>
        </div>
      </StyledMap>
    );
  } else {
    return (
      <StyledMap>
        <div>
          <PopoverStickOnHover
            placement="bottom"
            component={
              <div>
                <Map lat={props.lat} lng={props.lng} />
              </div>
            }
            onMouseEnter={() => {}}
            delay={200}
          >
            <a className="clickable underline">
              <img
                width="15"
                height="15"
                alt="rating start"
                src="/images/location.svg"
              />
              &nbsp;
              {props.address}
            </a>
          </PopoverStickOnHover>
        </div>
      </StyledMap>
    );
  }
}
