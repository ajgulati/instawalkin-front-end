import styled from 'styled-components';

export const MetricsBannerStyle = styled.div`
  .metrics-wrapper {
    .metrics-subtext {
      color: ${(props) => props.theme.color.white};
    }
    .dollar-sign {
      color: ${(props) => props.theme.color.pink};
      font-size: 2em;
    }
    .metrics-table {
      section {
        padding: 10px 0;
        span {
          font-family: Muli-SemiBold;
        }
      }
    }
  }
`;
