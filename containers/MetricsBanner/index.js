import React from 'react';
import PropTypes from 'prop-types';
import Container from 'react-bootstrap/Container';
import FlipNumbers from 'react-flip-numbers';
import { MetricsBannerStyle } from './metricsBanner.style';

export default function MetricsBanner({ metricsBannerData }) {
  const noBookings =
    metricsBannerData.bookings === 0 || metricsBannerData.bookings === null
      ? true
      : false;

  return (
    <MetricsBannerStyle>
      <section className="city-banner-section metrics-wrapper">
        <Container className="text-center">
          <h2 className="title">Your Business Can Thrive!</h2>
          <h3 className="modal-title metrics-subtext">
            Since {metricsBannerData.date}, RMT's in our network added{' '}
          </h3>
          {!noBookings && (
            <div className="row metrics-table">
              <div className="col-12 col-sm-6">
                <FlipNumbers
                  height={40}
                  width={40}
                  duration={2.8}
                  color="#ff7271"
                  delay={2.25}
                  background="inherit"
                  play
                  perspective={320}
                  numbers={metricsBannerData.bookings.toString()}
                />
                <h6 className="modal-title metrics-subtext">Appointments</h6>
              </div>
              <div className="col-12 col-sm-6">
                <div className="row align-items-center justify-content-center">
                  <span className="dollar-sign">$</span>
                  <FlipNumbers
                    height={40}
                    width={40}
                    duration={2.8}
                    delay={2.25}
                    color="#ff7271"
                    background="inherit"
                    play
                    perspective={320}
                    numbers={metricsBannerData.amount_earned}
                  />
                </div>
                <h6 className="modal-title metrics-subtext">Revenue</h6>
              </div>
            </div>
          )}
        </Container>
      </section>
    </MetricsBannerStyle>
  );
}

MetricsBanner.propTypes = {
  metricsBannerData: PropTypes.object,
};
