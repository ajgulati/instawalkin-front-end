import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import PageLoader from 'components/Views/PageLoader';
import isEmpty from 'lodash.isempty';
import { getEmailSignedUrl } from 'redux/app/actions';
import CustomModal from 'components/Base/Modal';
import qs from 'qs';
import Booked from 'containers/Booked';
export default function ThrivrFormFlows() {
  // States
  const loaderCount = useSelector((state) => state.app.loaderCount);
  const dispatch = useDispatch();
  const router = useRouter();
  let [modalConfirmShow, setModalConfirmShow] = useState(false);
  let [bookData, setBookData] = useState({});

  // Mount
  useEffect(() => {
    if (!isEmpty(router) && !isEmpty(router.query)) {
      fetchEmailRes();
    }
  }, []);

  const fetchEmailRes = async () => {
    let payload = {
      url: `expires=${router?.query?.expires}&param=${router?.query?.param}&signature=${router?.query?.signature}`,
    };
    const { data, status } = await dispatch(getEmailSignedUrl(payload));
    if (status) {
      setModalConfirmShow(true);
      if (data?.data?.['covid-form']) {
        const urlData = qs.parse(data?.data?.['covid-form']?.covidFormAPIUrl, {
          ignoreQueryPrefix: true,
        });
        setBookData({
          therapist_name: urlData.therapist_name,
          booking_slug: router?.query?.param,
          lessThan24: data?.data?.lessThan24,
        });
      }
    }
  };

  return (
    <PageLoader visible={loaderCount <= 0 ? false : true}>
      <div className="content-wrapper">
        <Head>
          <title>Thrivr Form Flows - Thrivr</title>
        </Head>
        <CustomModal
          show={modalConfirmShow}
          onHide={() => console.log('')}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          backdrop="static"
          className="modal-cancle desktop-alt massage-confirm"
          header={
            <>
              <h5 className="modal-title d-sm-block" id="exampleModalLabel">
                Massage Confirmed
              </h5>
            </>
          }
          body={<Booked params={router?.query?.param} bookData={bookData} />}
        />
      </div>
    </PageLoader>
  );
}
