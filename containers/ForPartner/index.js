import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import dynamic from 'next/dynamic';
import { useSelector } from 'react-redux';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Button from 'components/Base/Button';
import Input from 'components/Base/Input';
import { emailRegex } from 'util/config';
import PageLoader from 'components/Views/PageLoader';
import { ctaRecordLogger, getCityBanner } from 'services';
import isEmpty from 'lodash.isempty';
import InputGroup from 'components/Base/InputGroup';
import Footer from 'components/Footer';
import Header from 'components/Header';
import PreLoadComponent from 'components/Views/Common/PreLoadComponent';
// import FaqComponent from 'components/Views/Common/faq';
// import DescriptionComponent from 'components/Views/Common/description';
import Carousel, { CarouselItem } from 'components/Base/Carousel';
import TherapistStyleWrapper from './forPartner.style';
import Link from 'next/link';
import { useRouter } from 'next/router';
import YouTube from 'react-youtube';
// import MetricsBanner from 'containers/MetricsBanner';

const opts = {
  height: '250',
  width: '410',
};

const EmailSuccessMessage = () => (
  <div className="success-submit-wrap">
    <p>Thank you for request, we will be in touch with you soon.</p>
  </div>
);

const DynamicMetricsBanner = dynamic(() => import('containers/MetricsBanner'), {
  ssr: false,
  loading: () => <PreLoadComponent />,
});

const DynamicDescriptionComponent = dynamic(
  () => import('components/Views/Common/description'),
  {
    ssr: false,
    loading: () => <PreLoadComponent />,
  }
);

const DynamicFaqComponent = dynamic(
  () => import('components/Views/Common/faq'),
  {
    ssr: false,
    loading: () => <PreLoadComponent />,
  }
);

export default function Therapists() {
  const loaderCount = useSelector((state) => state.app.loaderCount);
  const [bookEmail, setBookEmail] = useState({ value: '', touched: false });
  const [metricsData, setMetricsData] = useState();
  const [startedEmail, setStartedEmail] = useState({
    value: '',
    touched: false,
  });

  const [bookSuccess, setBookSuccess] = useState(false);

  const router = useRouter();
  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    let metricsRes = await getCityBanner();
    if (metricsRes.status === 200 && !isEmpty(metricsRes.data)) {
      setMetricsData(metricsRes.data.data);
    }
  };

  const handleApplyRMT = () => {
    window.open('https://forms.gle/tYvrgS4Mm4MGpaRm6', '_blank');
  };
  const handleBookDemoClick = (data) => {
    window.dataLayer.push({
      event: 'demoClick',
      event_label: data,
      event_category: 'clicks',
      data: null,
    });
  };

  // Form Submit
  const handleBookSubmit = (event) => {
    event.preventDefault();
    if (!bookEmail.value || !emailRegex.test(bookEmail.value)) {
      setBookEmail({ ...bookEmail, touched: true });
      return;
    }
    setBookSuccess(true);
    router.push(`/request-demo?email=${bookEmail?.value}`);
    setTimeout(() => {
      setBookSuccess(false);
      setBookEmail({ value: '', touched: false });
    }, 3000);
  };

  const handleBookChange = (event) => {
    let value = event.target.value;
    if (/@/.test(value)) {
      ctaRecordLogger({ email: value, type: 'L' });
    }
    setBookEmail({ ...bookEmail, value, touched: true });
  };

  const handleBookBlur = () => {
    setBookEmail({ ...bookEmail, touched: true });
  };

  const handleStartedSubmit = (event) => {
    event.preventDefault();
    if (!startedEmail.value || !emailRegex.test(startedEmail.value)) {
      setStartedEmail({ ...startedEmail, touched: true });
      return;
    }
    setBookSuccess(true);
    setTimeout(() => {
      setBookSuccess(false);
      setStartedEmail({ value: '', touched: false });
    }, 2200);
  };

  const handleStartedChange = (event) => {
    let value = event.target.value;
    if (/@/.test(value)) {
      ctaRecordLogger({ email: value, type: 'L' });
    }
    setStartedEmail({ ...startedEmail, value, touched: true });
  };

  const handleStartedBlur = () => {
    setStartedEmail({ ...startedEmail, touched: true });
  };

  return (
    <PageLoader visible={loaderCount <= 0 ? false : true}>
      <TherapistStyleWrapper>
        <Head>
          <meta
            property="og:title"
            content="Thrivr - Online marketplace and practice management software for registered massage therapists"
          />
          <meta
            property="og:description"
            content="Expand and enhance your practice through our online marketplace and practice management software for registered massage therapists "
          />
          <title>
            Grow your massage practice with Thrivr&apos;s solution for getting
            bookings and filling up spots.
          </title>
        </Head>

        <Header className="header-white" activeLink="For therapist" />

        {/* Banner Section */}
        <section className="tan-banner d-flex">
          <div className="container-fluid m-auto">
            <div className="row">
              <div className="col-md-5 m-auto">
                <div className="banner-details">
                  <h2 className="main-title">
                    A Digital
                    <br />
                    Marketing Solution
                    <br />
                    <span className="font-bold">
                      {' '}
                      For <abbr title="Registerd Massage Therapists">RMTs</abbr>
                    </span>
                  </h2>
                  <p>
                    Thrivr is an extension to your clinic software that works in
                    the background to fill your openings.
                  </p>
                  <Button varient="primary" onClick={() => handleApplyRMT()}>
                    Apply to be on the network
                  </Button>
                </div>
              </div>
              <div className="col-md-7 m-auto text-right img-box">
                <img
                  alt="big pink circle"
                  src="/images/big_pink_circle.png"
                  className="w-100 img-background"
                />
                <img
                  alt="iPad mockup"
                  src="/images/ipad_image.png"
                  className="w-100 img-desktop d-none d-sm-block"
                />
                <img
                  alt="iPhone mockup"
                  src="/images/phone_image.png"
                  className="img-mobile"
                />
              </div>
            </div>
          </div>
        </section>

        {/* Video Section */}

        <section className="video-section">
          <Container className="text-center">
            <h3 className="title">How does Thrivr work?</h3>
            <div className="video">
              <YouTube
                opts={opts}
                containerClassName={'youtubeContainer'}
                videoId="3FjJ6NX7j0k"
              />
            </div>
          </Container>
        </section>

        {/* Description Section */}
        <DynamicDescriptionComponent>
          <Container className="text-center">
            <h3 className="title">Why Thrivr?</h3>
            <div className="row div-wrapper">
              <div className="col-lg-4 text-center">
                <img
                  width="75"
                  height="74"
                  alt="attract clients"
                  src="/images/attract.svg"
                />
                <h2 classNamme="title">Attract clientele</h2>
                <p>
                  Increase your visibility by creating a Thrivr
                  profile—displaying your valuable experience to hundreds of
                  potential clients.
                </p>
              </div>

              <div className="col-lg-4 text-center">
                <img
                  width="75"
                  height="74"
                  alt="build your massage practice "
                  src="/images/build.svg"
                />
                <h2 classNamme="title">Build relationships</h2>
                <p>
                  Turn your new clients into regulars through customized and
                  targeted client communications.
                </p>
              </div>

              <div className="col-lg-4 text-center">
                <img
                  width="75"
                  height="74"
                  alt="earn more"
                  src="/images/earn.svg"
                />
                <h2 classNamme="title">Earn in your sleep</h2>
                <p>
                  Thrivr is an extension to your clinic software that works in
                  the background to effortlessly fill your openings—let us take
                  the headache out of your marketing!
                </p>
              </div>
            </div>
          </Container>
        </DynamicDescriptionComponent>
        {/* City Banner Section */}
        {!isEmpty(metricsData) && (
          <DynamicMetricsBanner metricsBannerData={metricsData} />
        )}

        <DynamicDescriptionComponent>
          <Container className="text-center">
            <h3 className="title">
              How Thrivr works for Therapists in our Network
            </h3>
            <div className="row div-wrapper">
              <div className="col-lg-4 text-center">
                <img
                  width="75"
                  height="74"
                  alt="support via digital marketing"
                  src="/images/support.svg"
                />
                <h3 classNamme="title">We support</h3>
                <p>
                  your growth through our advanced digital marketing
                  capabilities.
                </p>
              </div>

              <div className="col-lg-4 text-center">
                <img
                  width="75"
                  height="74"
                  alt="secure payment"
                  src="/images/secure.svg"
                />
                <h3 classNamme="title">We secure</h3>
                <p>
                  payment in advance of client arrival, and cancellation fees
                  help to protect your valuable time.
                </p>
              </div>

              <div className="col-lg-4 text-center">
                <img
                  width="75"
                  height="74"
                  alt="deliver clients"
                  src="/images/deliver.svg"
                />
                <h3 classNamme="title">We deliver</h3>
                <p>
                  a seamless experience for you and the client, including
                  payment, receipts, and cancellation protection.
                </p>
              </div>
            </div>
          </Container>
        </DynamicDescriptionComponent>

        {/* Download Section */}
        <section className="download-section email-section">
          <Container className="text-center">
            <p className="title h1">
              See <span className="font-weight-bold">Your</span> Business
              Thrive!
              <br />
              <small className="text-white">
                Only pay for the clients we refer to you!
              </small>
            </p>

            <div>
              <Input
                type="email"
                placeholder="Your email…"
                name="startedEmail"
                value={startedEmail.value}
                onChange={handleStartedChange}
                onBlur={handleStartedBlur}
                isInvalid={
                  startedEmail.touched &&
                  (!startedEmail.value || !emailRegex.test(startedEmail.value))
                    ? true
                    : false
                }
              />
              <Button varient="primary" onClick={handleStartedSubmit}>
                Get started
              </Button>
              {bookSuccess && <EmailSuccessMessage />}
            </div>
          </Container>
        </section>

        <DynamicDescriptionComponent>
          <Container className="text-center">
            <h3 className="title">Joining is as easy as 1-2-3!</h3>
            <div className="row div-wrapper">
              <div className="col-lg-4 text-center">
                <img
                  width="75"
                  height="74"
                  alt="Apply to be on thrivr"
                  src="/images/Apply.svg"
                />
                <h3 classNamme="title">Apply</h3>
                <p>Apply to join the Thrivr network.</p>
              </div>

              <div className="col-lg-4 text-center">
                <img
                  width="75"
                  height="74"
                  alt="Connect your scheduling software"
                  src="/images/connect.svg"
                />
                <h3 classNamme="title">Connect</h3>
                <p>
                  We connect you with hundreds of potential clients in your
                  area.
                </p>
              </div>

              <div className="col-lg-4 text-center">
                <img
                  width="75"
                  height="74"
                  alt="Let us make you practice thrive"
                  src="/images/thrive.svg"
                />
                <h3 classNamme="title">Thrive</h3>
                <p>Start earning to your full potential.</p>
              </div>
            </div>
            <div>
              <Button varient="primary" onClick={() => handleApplyRMT()}>
                Apply to be on the network
              </Button>
            </div>
          </Container>
        </DynamicDescriptionComponent>

        {/* Review Section */}
        <section className="review-section bg-light">
          <Container className="text-center">
            <h3 className="title pb-5">
              What Registered Massage Therapists in the network are saying
            </h3>
            <div className="row">
              <div className="col-lg-6 offset-lg-3 mb-5 ">
                <YouTube
                  opts={opts}
                  containerClassName={'youtubeContainer'}
                  videoId="giGC_VcgN6E"
                />
                <h5 className="text-left">Vanessa Rehbien, RMT</h5>
                <p className="text-left">8th St Chiro - Saskatoon</p>
              </div>
            </div>
          </Container>
        </section>

        {/* Pricing Section */}
        <section className="pricing-section" id="pricing">
          <Container>
            <h3 className="title text-center">Pricing</h3>
            <div className="row">
              <div className="col-sm-6 col-md-5 ml-auto box-wrapper">
                <div className="box">
                  <div className="title">Listing</div>
                  <div className="pkg">Free</div>
                  <ul className="list-unstyled">
                    <li>Free Profile Listing</li>
                    <li>Reviews</li>
                    <li>Analytics</li>
                    <li>24/7 Support</li>
                  </ul>
                  <div className="text-center box-btn">
                    <Link href="/request-demo">
                      <Button
                        onClick={() => handleBookDemoClick('listing')}
                        varient="primary"
                      >
                        Book A 1-on-1 Demo
                      </Button>
                    </Link>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-md-5 mr-auto box-wrapper">
                <div className="box box-dark">
                  <div className="title">Premium</div>
                  <div className="pkg">
                    15% <span>per transaction</span>
                  </div>
                  <span>1st Month is on us</span>
                  <ul className="list-unstyled">
                    <li>Listing features included</li>
                    <li>Scheduling Software</li>
                    <li>Calendar Sync with Google and Outlook (both ways)</li>
                    <li>Intake and COVID-19 Forms</li>
                    <li>Online Payments (Processing fee included)</li>
                    <li>Re-scheduling And Cancellation Fees</li>
                    <li>Free Ads on Google and Facebook</li>
                    <li>Automated Receipts</li>
                    <li>24/7 Support</li>
                  </ul>
                  <div className="text-center box-btn">
                    <Link href="/request-demo">
                      <Button
                        varient="primary"
                        onClick={() => handleBookDemoClick('premium')}
                      >
                        Book A 1-on-1 Demo
                      </Button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </Container>
        </section>

        {/* Faq Section */}
        <DynamicFaqComponent
          faqs={FaqArrConstant}
          title={'Frequently Asked Questions'}
        />

        {/* Footer Section */}
        <Footer />
      </TherapistStyleWrapper>
    </PageLoader>
  );
}

const FaqArrConstant = [
  {
    key: '0',
    title: `How does Thrivr work?`,
    description: (
      <>
        Thrivr is a digital marketing solution that allows you to display your
        openings to hundreds of prospective clients. We integrate with your
        existing scheduling software and display your openings across our
        network.
      </>
    ),
  },
  {
    key: '1',
    title: `What if I don’t have scheduling software?`,
    description: (
      <>
        If you don’t have scheduling software, we’ll provide it for you—no
        effort required on your end.
      </>
    ),
  },
  {
    key: '2',
    title: `How do I get paid?`,
    description: (
      <>
        Thrivr therapists are paid out via direct deposit on a semi-monthly
        basis (on the first business day after the 15th and the first business
        day after the 30th or 31st).
      </>
    ),
  },
  {
    key: '3',
    title: `Will I receive tips from my clients?`,
    description: (
      <>
        Yes! You will receive 100% of your tips, which will be deposited along
        with your Bi-weekly payments.
      </>
    ),
  },
  {
    key: '4',
    title: `Can I add someone else to my account?`,
    description: (
      <>
        Yes. If you would like to give a receptionist access to your account, we
        can do that easily.
      </>
    ),
  },
  {
    key: '5',
    title: `What’s the pricing plan like for Thrivr?`,
    description: (
      <>
        Try Thrivr premium free for 1 month or check out our free listing option
      </>
    ),
  },
  {
    key: '6',
    title: `What happens when a client reschedules or cancels their appointment?`,
    description: (
      <>
        <p>
          If a client reschedules their appointment within 24 hrs of the
          scheduled start time of that appointment, you will be paid 25% of the
          total original appointment rate.
        </p>
        <p>
          If a client cancels their appointment within 24 hrs of the scheduled
          appointment start time, you will be paid 75% of the total original
          appointment rate.
        </p>
      </>
    ),
  },
  {
    key: '7',
    title: `How does Thrivr integrate with my current scheduling software?`,
    description: (
      <>
        Once you integrate, Thrivr will know when you have an open slot and when
        you are booked, so that available times can be served up to clients. For
        some scheduling softwares, we can actually close the spot on your
        calendar once a client books that time slot. For others, we will
        manually book an appointment slot once it’s scheduled, or you can
        manually book it.
      </>
    ),
  },
  {
    key: '8',
    title: `How will I get notified when a new appointment is scheduled through Thrivr?`,
    description: <>You may choose to be notified via text, email or both.</>,
  },
  {
    key: '9',
    title: `Is there a buffer period of how soon a client can book an appointment with me?`,
    description: (
      <>
        Yes, it’s currently set to 1 hour. You will soon be able to set your own
        buffer window..
      </>
    ),
  },
  {
    key: '10',
    title: `Is there a buffer period after a massage?`,
    description: <>Yes and you can set your own buffer period.</>,
  },
];
