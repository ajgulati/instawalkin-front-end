import React, { useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useForm, Controller } from 'react-hook-form';
import Form from 'react-bootstrap/Form';
import { motion, AnimatePresence } from 'framer-motion';
import * as yup from 'yup';
import { passwordRegex, phoneRegex } from 'util/config';
import isEmpty from 'lodash.isempty';
import InputMask from 'react-input-mask';
import Button from 'components/Base/Button';
import Input from 'components/Base/Input';
import Container from 'react-bootstrap/Container';
import { useDispatch, useSelector } from 'react-redux';
import { SeperatorStyle } from 'components/StyleComponent';
import InputGroup from 'components/Base/InputGroup';
import PageLoader from 'components/Views/PageLoader';
import Footer from 'components/Footer';
import Header from 'components/Header';
import { signupGuest } from 'redux/auth/actions';

export default function VerifyGuestEmail() {
  const router = useRouter();
  const dispatch = useDispatch();
  const loaderCount = useSelector((state) => state.app.loaderCount);
  const errorBackend = useSelector((state) => state.auth.error);
  const [isPhoneRequired, setIsPhoneRequired] = useState(false);

  React.useEffect(() => {
    if (router?.query?.phone) {
      if (router?.query?.phone === 'Y') {
        setIsPhoneRequired(true);
      }
    }
  }, []);

  const Passwordschema = yup.object({
    is_phone: yup.boolean().optional().default(isPhoneRequired),
    phone: yup
      .string()
      .when('is_phone', {
        is: true,
        then: yup.string().required('Please enter the phonenumber'),
      })
      .matches(phoneRegex, 'Please enter valid phonenumber'),
    password: yup
      .string()
      .required('Please enter the password')
      .matches(passwordRegex, 'Password should be minimun 6 characters'),
  });

  const {
    register: passwordRegister,
    handleSubmit: handleSubmit,
    control,
    errors: passwordErrors,
    getValues,
    setValue,
  } = useForm({
    validationSchema: Passwordschema,
    defaultValues: {
      phone: undefined,
      password: '',
      is_phone: isPhoneRequired,
    },
  });

  const onSubmit = async (data) => {
    try {
      let payload = {
        password: data.password,
        phone: data.phone,
        grant_type: 'password',
        scope: '*',
      };

      await dispatch(signupGuest(payload, router?.asPath));
    } catch (error) {
      if (error?.response) {
        console.log('error', error?.response);
      }
    }
  };

  const handleBlur = (event) => {
    if (!isPhoneRequired && event?.[0]?.target.value === '') {
      setValue('phone', undefined);
    }
  };

  return (
    <PageLoader visible={loaderCount <= 0 ? false : true}>
      <div className="content-wrapper">
        <Head>
          <title>Guest Signup - Thrivr</title>
        </Head>

        {/* Header Section */}
        <Header className="form-header" />

        {/* Banner Section */}
        <section className="banner-wrapper d-flex">
          <Container className="m-auto position-relative">
            {/* Alert Error Message */}
            <AnimatePresence>
              {!isEmpty(errorBackend) &&
                typeof errorBackend === 'object' &&
                errorBackend !== null && (
                  <motion.div
                    className="alert-wrapper"
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }}
                    transition={{ duration: 0.3 }}
                    exit={{ opacity: 0 }}
                  >
                    {Object.values(errorBackend).map((error) =>
                      error.map((field) => (
                        <Alert variant={'error'} key={field}>
                          {field}
                        </Alert>
                      ))
                    )}
                  </motion.div>
                )}
            </AnimatePresence>

            <div className="form-details">
              <h2 className="main-title d-none d-sm-none d-md-block">
                Easiest way to book a massage
              </h2>

              <div className="form-wrapper">
                <Form className="login" onSubmit={handleSubmit(onSubmit)}>
                  <div className="form-title ml-auto">Guest Signup</div>
                  <SeperatorStyle />
                  <InputGroup isCustom>
                    <Input
                      type="password"
                      name="password"
                      title="New Password"
                      placeholder="New Password"
                      refProps={passwordRegister}
                      isInvalid={
                        !isEmpty(passwordErrors) &&
                        !isEmpty(passwordErrors.password)
                          ? true
                          : false
                      }
                    />
                    {!isEmpty(passwordErrors) && passwordErrors.password && (
                      <Form.Control.Feedback type="invalid">
                        {passwordErrors.password.message}
                      </Form.Control.Feedback>
                    )}
                  </InputGroup>
                  <InputGroup isCustom>
                    <Controller
                      mask="+1 (999) 999-9999"
                      className="form-control"
                      placeholder="Your phone number"
                      onBlur={handleBlur}
                      as={
                        <InputMask>
                          {(maskProps) => (
                            <Input
                              {...maskProps}
                              isInvalid={
                                !isEmpty(passwordErrors) &&
                                !isEmpty(passwordErrors.phone)
                              }
                              refProps={passwordRegister}
                            />
                          )}
                        </InputMask>
                      }
                      control={control}
                      name="phone"
                    />
                    {!isEmpty(passwordErrors) && passwordErrors.phone && (
                      <Form.Control.Feedback type="invalid">
                        {passwordErrors.phone.message}
                      </Form.Control.Feedback>
                    )}
                  </InputGroup>

                  <Button className="mob-full" type="submit">
                    Submit
                  </Button>
                </Form>
              </div>
            </div>
          </Container>
        </section>

        {/* Footer Section */}
        <Footer />
      </div>
    </PageLoader>
  );
}
