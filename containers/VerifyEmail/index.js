import React from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';
import PageLoader from 'components/Views/PageLoader';
import { verifyEmailService } from 'services';
import Footer from 'components/Footer';
import { LayoutDiv } from 'components/StyleComponent';
import Header from 'components/Header';
import { verifyEmailAction } from 'redux/app/actions';

export default function VerifyEmail() {
  const router = useRouter();
  const dispatch = useDispatch();
  const [isAccept, setIsAccept] = React.useState(false);

  React.useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const data = await verifyEmailService(router?.asPath);
      if (data?.data?.message) {
        await dispatch(verifyEmailAction(data?.data?.message));
      }
    } catch (error) {
      console.log(error?.response);
      if (
        error?.response?.data?.errors &&
        typeof error?.response?.data?.errors === 'string'
      ) {
        await dispatch(verifyEmailAction(error?.response?.data?.errors));
      }
    }
    router.replace('/');
  };

  return (
    <React.Fragment>
      <Head>
        <meta
          property="og:title"
          content="Verify Email | Thrivr - On demand massage service in Saskatoon and Regina, Saskatchewan."
        />
        <meta
          property="og:description"
          content="Let’s get in touch! Send us your questions and we’ll respond as soon as possible. Thrivr offers a spa-quality massage experience, allowing you to book highly rated massage therapists within a few clicks."
        />
        <title>Verify Email - Thrivr</title>
      </Head>
      <PageLoader visible={true}>
        <Header className="header-white" activeLink="Contact" />
        <LayoutDiv></LayoutDiv>
      </PageLoader>

      {/* Footer Section */}
      <Footer />
    </React.Fragment>
  );
}
