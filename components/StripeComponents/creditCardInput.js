import react, { useState } from 'react';
import {
  CardElement,
  CardNumberElement,
  useElements,
  useStripe,
} from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import Button from 'components/Base/Button';
import Modal from 'react-bootstrap/Modal';
import { ModalBody, ModalFooter } from 'react-bootstrap';
export const StripeCreditCardInput = ({
  show,
  handleOnCreate,
  handleOnClose,
  showCancelationPolicyModal = null,
  showCancelationPolicyText = false,
}) => {
  const stripe = useStripe();
  const elements = useElements();
  const handleOnHide = () => {
    handleOnClose();
  };

  const CardInputFooter = ({ handleOnHide, handleOnCreate }) => {
    return (
      <div className="d-flex justify-content-end align-items-center ">
        <Button
          className="btn btn-white d-none d-sm-block"
          onClick={() => {
            if (showCancelationPolicyText) {
              // trigger event when cancelling the addition of a card within the checkout process
              window.dataLayer.push({
                event: 'CheckoutProcessCancellingCC',
                event_category: 'clicks',
                data: null,
              });
            }

            handleOnHide(true);
          }}
        >
          {' '}
          Cancel
        </Button>
        <Button onClick={handleOnCreate}>
          {' '}
          {showCancelationPolicyText ? 'Confirm Booking' : 'Add'}
        </Button>
      </div>
    );
  };
  return (
    <div>
      <Modal
        className="modal-cancle desktop-alt"
        centered
        show={show}
        onHide={handleOnHide}
      >
        <Modal.Header>
          <h4 className="">
            {showCancelationPolicyText
              ? 'To hold your spot we require a credit card. Your card will not be charged.'
              : 'Add a new credit card'}{' '}
          </h4>
        </Modal.Header>

        <ModalBody>
          <div className="mt-3">
            <CardInputBody />
          </div>
        </ModalBody>

        <ModalFooter>
          <CardInputFooter
            handleOnHide={handleOnHide}
            handleOnCreate={handleOnCreate}
          ></CardInputFooter>

          {showCancelationPolicyText ? (
            <a
              className="text-muted"
              onClick={() => showCancelationPolicyModal(true)}
            >
              <u> 24 Hour Cancellation and Rescheduling policy in effect </u>{' '}
            </a>
          ) : null}
        </ModalFooter>
      </Modal>
    </div>
  );
};

const CardInputBody = () => {
  return (
    <div>
      <CardElement
        options={{
          style: {
            base: {
              fontSize: '16px',
              color: '#424770',
              '::placeholder': {
                color: '#aab7c4',
              },
            },
            invalid: {
              color: '#9e2146',
            },
          },
          hidePostalCode: true,
        }}
      ></CardElement>
    </div>
  );
};
