import React, { useState } from 'react';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import {
  checkAuth,
  googleOauthInitialize,
  fbOauthInitialize,
  userEmailUpdate,
} from 'redux/auth/actions';
import { clearAppErrorAction } from 'redux/app/actions';
import isEmpty from 'lodash.isempty';
import ErrorAlert from 'components/ErrorAlert';
import SuccessAlert from 'components/SuccessAlert';
import { resendEmailService } from 'services';
import CustomModal from 'components/Base/Modal';
import { InputGroup } from 'react-bootstrap';
import Button from 'components/Base/Button';
import Input from 'components/Base/Input';

export default function AuthCheck() {
  const router = useRouter();
  const dispatch = useDispatch();
  const authenticated = useSelector((state) => state.auth.authenticated);
  const errorAppReducer = useSelector((state) => state.app.error);
  const userProfile = useSelector((state) => state.auth.userProfile);
  const [emailVerified, setEmailVerified] = useState(true);
  const [emailSentMessage, setEmailSentMessage] = useState(null);
  const [modalShow, setModalShow] = useState(false);

  const userEmail = useSelector((state) => state.auth.userProfile.email);
  const [emailInput, setEmailInput] = useState('');
  const [emailError, setEmailError] = useState('');
  const [showedEmailVerification, setShowedEmailVerification] = useState(true);
  const [showEmailVerification, setShowEmailVerification] = useState(false);

  React.useEffect(() => {
    var tmp = JSON.parse(
      localStorage.getItem('showed_email_verification_message')
    );
    var showNotification = false;
    if (tmp) {
      var localStorageDate = new Date(tmp.date);
      var currentDate = new Date();

      if (
        localStorageDate.toDateString() == currentDate.toDateString() &&
        tmp.showed
      ) {
        showNotification = true;
      }
    }

    setShowedEmailVerification(showNotification);
  }, []);

  React.useEffect(() => {
    tokenExpiryCheck();
    // Google Gapi Load
    if (!isEmpty(window)) {
      const script = document.createElement('script');
      script.src = 'https://apis.google.com/js/client.js';
      script.onload = () => {
        let { gapi } = window;
        gapi.load('auth2', () => {
          let auth2 = gapi.auth2.init({
            client_id: process.env.GOOGLE_CLIENT_ID,
          });

          dispatch(googleOauthInitialize(auth2));
        });
      };

      document.body.appendChild(script);
    }

    // Facebook FB Load
    if (!isEmpty(window)) {
      window.fbAsyncInit = function () {
        FB.init({
          appId: process.env.FACEBOOK_APP_ID,
          cookie: false,
          xfbml: true,
          version: 'v9.0',
        });
        dispatch(fbOauthInitialize(window.FB));
      };
      // Load the SDK asynchronously
      (function (d, s, id) {
        var js,
          fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = '//connect.facebook.net/en_US/sdk.js';
        fjs.parentNode.insertBefore(js, fjs);
      })(document, 'script', 'facebook-jssdk');
    }
  }, []);

  React.useEffect(() => {
    if (!isEmpty(userProfile)) {
      if (
        isEmpty(userProfile.verified_at) &&
        (showedEmailVerification == null || !showedEmailVerification)
      ) {
        if (!showedEmailVerification) {
          localStorage.setItem(
            'showed_email_verification_message',
            JSON.stringify({ showed: true, date: new Date() })
          );
        }
        setShowedEmailVerification(true);
        setEmailVerified(false);
        setShowEmailVerification(true);
      } else {
        setEmailVerified(true);
      }
    }
  }, [userProfile]);

  React.useEffect(() => {
    /**
     * Email Sent Check Runtime to convert it to initial state.
     */
    if (!isEmpty(emailSentMessage)) {
      setTimeout(() => {
        setEmailSentMessage(null);
      }, 3000);
    }
  }, [emailSentMessage]);

  React.useEffect(() => {
    if (showEmailVerification != null) {
      setTimeout(() => {
        setShowEmailVerification(false);
      }, 20000);
    }
  }, [showEmailVerification]);

  const tokenExpiryCheck = async () => {
    // Current Token Check ACTION
    await dispatch(checkAuth(authenticated, router.pathname || '/'));
  };

  React.useEffect(() => {
    if (!isEmpty(errorAppReducer)) {
      setTimeout(() => {
        dispatch(clearAppErrorAction());
      }, 3000);
    }
  }, [errorAppReducer]);

  const handleResend = async () => {
    const res = await resendEmailService();
    if (res.status === 200) {
      setEmailSentMessage(res.data.message);
    }
  };

  // Add missing email to profile
  const addEmail = async () => {
    let payload = {
      email: emailInput,
    };

    let res = await dispatch(userEmailUpdate(payload));

    if (res.status) {
      // Show success message
      setEmailSentMessage('Your email has been added');

      // Hide modal and clear errors
      setModalShow(false);
      setEmailError(null);
    } else {
      if (res.error) {
        setEmailError(res.error);
      } else {
        setEmailError('An unexpected error occurred.');
      }
    }
  };

  const handleInputChange = (e) => {
    setEmailInput(e.target.value);
  };

  return !isEmpty(userProfile) ? (
    !userEmail ? (
      <>
        <SuccessAlert message={emailSentMessage} />

        <CustomModal
          show={modalShow}
          onHide={() => setModalShow(false)}
          size="lg"
          className="modal-cancle desktop-alt"
          header={
            <h5 className="modal-title" id="exampleModalLabel">
              If you would like to receive an email to confirm your booking,
              please enter one now.
            </h5>
          }
          body={
            <InputGroup isCustom>
              <Input
                type="email"
                placeholder="Email"
                name="email"
                onChange={(e) => handleInputChange(e)}
              />
              {emailError && (
                <p
                  className="error-message"
                  style={{
                    color: '#ff0033',
                    textAlign: 'center',
                    width: '100%',
                    marginTop: '8px',
                    fontSize: '14px',
                  }}
                >
                  {emailError}
                </p>
              )}
            </InputGroup>
          }
          footer={
            <Button className="btn mobile-btn" onClick={addEmail}>
              Update Email
            </Button>
          }
        />
      </>
    ) : (
      !emailVerified && (
        <>
          {/* Alert Email Resend Success Message */}
          <SuccessAlert message={emailSentMessage} />
          <SuccessAlert
            message={
              showEmailVerification ? (
                <div>
                  Please verify your email.
                  <a onClick={handleResend}>
                    <u>Click to resend</u>
                  </a>
                </div>
              ) : (
                ''
              )
            }
            className="error-verified"
          />
        </>
      )
    )
  ) : null;
}
