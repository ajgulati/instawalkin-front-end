import React from 'react';
import PropTypes from 'prop-types';
import Alert from 'react-bootstrap/Alert';
import { motion, AnimatePresence } from 'framer-motion';
import cx from 'classnames';

/**
 * Custom Error Message Alert
 * @param message - Actual Message
 */
export default function CustomErrorAlert({ message, className }) {
  return (
    <AnimatePresence>
      <motion.div
        className={cx('alert-global-success-wrapper', className)}
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 0.3 }}
        exit={{ opacity: 0 }}
      >
        <Alert variant={'danger'}>{message}</Alert>
      </motion.div>
    </AnimatePresence>
  );
}

CustomErrorAlert.propTypes = {
  message: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  className: PropTypes.string,
};
