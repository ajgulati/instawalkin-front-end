import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Router, { useRouter } from 'next/router';
import Navbar from 'react-bootstrap/Navbar';
import Dropdown from 'react-bootstrap/Dropdown';
import { useDispatch, useSelector } from 'react-redux';
import Nav from 'react-bootstrap/Nav';
import cx from 'classnames';
import { useWindowWidth } from '@react-hook/window-size';
import Link from 'next/link';
import Button from 'components/Base/Button';
import StyledHeader from './header.style.js';
import { logout } from 'redux/auth/actions';
import { SeperatorStyle } from '../StyleComponent';
import isEmpty from 'lodash.isempty';
import { getPathIndexFromRedirect } from 'util/config';
import Image from 'next/image';
// import ReactTooltip from 'react-tooltip';

// import Badge from 'react-bootstrap/Badge';

const CustomToggle = React.forwardRef(
  ({ children, onClick, className }, ref) => (
    <a
      href=""
      className={cx(className)}
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      {children}
    </a>
  )
);

CustomToggle.displayName = 'CustomToggle';

CustomToggle.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  onClick: PropTypes.func,
};

export default function CustomHeader({ className, activeLink, hamburgerIcon }) {
  let dispatch = useDispatch();
  let widthWindow = useWindowWidth();
  const router = useRouter();
  const authenticated = useSelector((state) => state.auth.authenticated);
  const userRewards = useSelector((state) => state.auth.userRewards);
  const userRewardsList = useSelector((state) => state.auth.userRewardsList);
  const userProfile = useSelector((state) => state.auth.userProfile);

  let [navExpand, setNavExpand] = useState(false);
  let [selectedLink, setSelectedLink] = useState(activeLink);

  const handleToggle = (data) => {
    setNavExpand(data);
  };

  const handleRedrect = () => {
    handleToggle(false);
    router.push({
      pathname: `/promo-code`,
    });
  };

  const handleInternalNavigation = () => {
    if (widthWindow <= 767) {
      handleToggle(false);
    }
  };

  Router.events.on('hashChangeComplete', (url) => {
    if (url && url.includes('#faq')) {
      setSelectedLink('faq');
    }
    if (url && url.includes('#pricing')) {
      setSelectedLink('pricing');
    }
  });

  useEffect(() => {
    if (!isEmpty(router) && router.asPath.includes('#faq')) {
      setSelectedLink('faq');
    }
    if (!isEmpty(router) && router.asPath.includes('#pricing')) {
      setSelectedLink('pricing');
    }
  }, [router, selectedLink]);

  const handleLogout = () => {
    dispatch(logout(router.asPath || '/'));
  };

  const handleCollapse = (data) => {
    if (!isEmpty(router) && router.asPath.includes(data.toLowerCase())) {
      setNavExpand(false);
    }
  };

  let newAsPath = '/';
  if (!isEmpty(router) && !isEmpty(router.query.redirectFrom)) {
    const index = getPathIndexFromRedirect(router);
    newAsPath = router.asPath.substring(index);
  }

  if (typeof window === 'undefined') {
    global.window = {};
  }

  if (window.innerWidth <= 992 && authenticated) {
    return (
      <StyledHeader className={cx(className, { open: navExpand })}>
        <Navbar expand="lg" onToggle={handleToggle} expanded={navExpand}>
          <Link href="/">
            <a className="navbar-brand" aria-label="Thrivr Home">
              <div className="logo-header" />
            </a>
          </Link>

          <Navbar.Toggle aria-controls="basic-navbar-nav">
            <span className="navbar-toggler-icon">
              {navExpand ? (
                <img
                  width="28"
                  height="28"
                  alt="Navbar Pink"
                  src="/images/nav-pink.svg"
                  className="pink"
                />
              ) : hamburgerIcon === 'white' ? (
                <img
                  width="28"
                  height="28"
                  alt="Navbar Pink"
                  src="/images/nav.svg"
                  className="white"
                />
              ) : (
                <img
                  width="28"
                  height="28"
                  alt="Navbar Pink"
                  src="/images/nav-pink.svg"
                  className="pink"
                />
              )}
            </span>
          </Navbar.Toggle>

          <Navbar.Collapse
            className={authenticated ? 'loged-in' : ''}
            id="basic-navbar-nav"
          >
            {/*<Nav.Item className="user-profile mt-3">
                <Dropdown>
                      <span className="profile-img">
                        {!isEmpty(userProfile) && userProfile.avatar ? (
                            <img alt="User avatar" src={userProfile.avatar} />
                        ) : (
                            <img alt="User avatar" src="/images/avatar.png" />
                        )}
                      </span>

                  <div className="navbar-text  ">
                    {!isEmpty(userProfile)
                        ? `${userProfile.firstname} ${userProfile.lastname}`
                        : 'User'}
                  </div>
                </Dropdown>

              </Nav.Item>*/}

            <Nav.Item className="ml-auto mt-3 user-profile">
              {userRewards?.reward ? (
                <div className="gift-img">
                  <div className="item" onClick={handleRedrect}>
                    <span className="notify-badge">
                      {userRewardsList?.length}
                    </span>
                    <Image
                      // layout="fill"
                      layout="fixed"
                      width={30}
                      height={30}
                      src="/images/gift56.png"
                    />
                  </div>
                  <Link href="/promo-code" onClick={() => handleToggle(false)}>
                    {window.innerWidth <= 320 ? (
                      <a>
                        Get $10 off
                        <span className="badge badge-pill badge-warning count-notif green">
                          New
                        </span>
                      </a>
                    ) : (
                      <a>
                        {' '}
                        {userRewards?.reward_text}
                        <span className="badge badge-pill badge-warning count-notif green">
                          New
                        </span>
                      </a>
                    )}
                  </Link>
                </div>
              ) : (
                <div className="gift-img" onClick={handleRedrect}>
                  <Image
                    // layout="fill"
                    layout="fixed"
                    width={30}
                    height={30}
                    src="/images/gift56.png"
                  />

                  <Link href="/promo-code" onClick={() => handleToggle(false)}>
                    {window.innerWidth <= 320 ? (
                      <a>
                        Get $10 off
                        <span className="badge badge-pill badge-warning count-notif green">
                          New
                        </span>
                      </a>
                    ) : (
                      <a>
                        {' '}
                        {userRewards?.reward_text}
                        <span className="badge badge-pill badge-warning count-notif green">
                          New
                        </span>
                      </a>
                    )}
                  </Link>
                </div>
              )}
            </Nav.Item>

            <Nav className="w-100">
              <Nav.Item className="mt-2" onClick={handleInternalNavigation}>
                <Link href="/order-history">
                  <a
                    className={cx('nav-link', {
                      active: 'appointments' === selectedLink,
                    })}
                  >
                    Appointments
                  </a>
                </Link>
              </Nav.Item>
              <Nav.Item onClick={handleInternalNavigation}>
                <Link href="/profile">
                  <a
                    className={cx('nav-link', {
                      active: 'settings' === selectedLink,
                    })}
                  >
                    Settings
                  </a>
                </Link>
              </Nav.Item>
              <Nav.Item onClick={handleInternalNavigation}>
                <Link href="/intake-form">
                  <a
                    className={cx('nav-link', {
                      active: 'intake form' === selectedLink,
                    })}
                  >
                    Intake forms
                  </a>
                </Link>
              </Nav.Item>

              {activeLink !== 'For therapist' ? (
                <Nav.Item>
                  <Dropdown className="menu-icon">
                    <Dropdown.Toggle className="nav-link" as={CustomToggle}>
                      For therapist
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Link href="/become-a-partner">
                        <a className="dropdown-item">Become a Partner</a>
                      </Link>
                      <Link href="/become-a-partner#pricing">
                        <a className="dropdown-item">Pricing</a>
                      </Link>
                      <a className="dropdown-item" href={process.env.LOGIN_URL}>
                        Partner Login
                      </a>
                    </Dropdown.Menu>
                  </Dropdown>
                </Nav.Item>
              ) : (
                <>
                  <Nav.Item>
                    <a
                      className={cx('nav-link', {
                        active: 'Partner Login' === selectedLink,
                      })}
                      href={process.env.LOGIN_URL}
                    >
                      Partner Login
                    </a>
                  </Nav.Item>
                  <Nav.Item onClick={handleInternalNavigation}>
                    <Link href="#pricing">
                      <a
                        className={cx('nav-link', {
                          active: 'pricing' === selectedLink,
                        })}
                      >
                        Pricing
                      </a>
                    </Link>
                  </Nav.Item>
                </>
              )}
              <Nav.Item onClick={handleInternalNavigation}>
                <Link href={activeLink === 'For therapist' ? '#faq' : '/#faq'}>
                  <a
                    className={cx('nav-link', {
                      active: 'faq' === selectedLink,
                    })}
                  >
                    FAQ
                  </a>
                </Link>
              </Nav.Item>
              <Nav.Item>
                <Link href="/contact-us">
                  <a
                    className={cx('nav-link', {
                      active: 'Contact' === selectedLink,
                    })}
                  >
                    Contact
                  </a>
                </Link>
              </Nav.Item>

              <Nav.Item>
                <Link href="#">
                  <a
                    onClick={handleLogout}
                    className={cx('nav-link', {
                      active: 'logout' === selectedLink,
                    })}
                  >
                    Log out
                  </a>
                </Link>
              </Nav.Item>
              {/* commented out temporarily */}
              {/* <Nav.Item>
              <Link href="">
                <a
                  className={cx('nav-link', {
                    active: 'Blog' === selectedLink,
                  })}
                >
                  Blog
                </a>
              </Link>
            </Nav.Item> */}

              {!authenticated ? (
                <>
                  <Nav.Item className="ml-auto">
                    <Link
                      href={`/login?redirectFrom=${
                        router?.pathname === '/login' ||
                        router?.pathname === '/signup'
                          ? !isEmpty(router.query) && router.query.redirectFrom
                            ? newAsPath
                            : '/'
                          : router?.asPath || '/'
                      }`}
                    >
                      <a
                        className={cx('nav-link', {
                          active: 'Login' === selectedLink,
                        })}
                        onClick={() => handleCollapse('login')}
                      >
                        Log in
                      </a>
                    </Link>
                  </Nav.Item>
                  <Nav.Item className="mr-0">
                    {activeLink === 'For therapist' ? (
                      <Link href="/request-demo">
                        <Button className="last-btn">Book A demo</Button>
                      </Link>
                    ) : (
                      <Link
                        href={`/signup?redirectFrom=${
                          router?.pathname === '/login' ||
                          router?.pathname === '/signup'
                            ? !isEmpty(router.query) &&
                              router.query.redirectFrom
                              ? newAsPath
                              : '/'
                            : router?.asPath || '/'
                        }`}
                      >
                        <a>
                          <Button
                            className="last-btn"
                            onClick={() => {
                              /* FBP signup */
                              // window.fbq('trackCustom', 'signup', {
                              //   actionItem: 'button',
                              //   data: null,
                              //   event_label: 'standard',
                              //   event_category: 'impressions',
                              // });

                              // /* Gtag signup */
                              // window.gtag('event', 'signup', {
                              //   event_category: 'impressions',
                              //   event_label: 'standard',
                              //   actionItem: 'button',
                              //   data: null,
                              // });
                              handleCollapse('signup');
                            }}
                          >
                            Sign up
                          </Button>
                        </a>
                      </Link>
                    )}
                  </Nav.Item>
                </>
              ) : (
                <></>
              )}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </StyledHeader>
    );
  } else {
    return (
      <StyledHeader className={cx(className, { open: navExpand })}>
        <Navbar expand="lg" onToggle={handleToggle} expanded={navExpand}>
          <Link href="/">
            <a className="navbar-brand" aria-label="Thrivr Home">
              <div className="logo-header" />
            </a>
          </Link>
          <Navbar.Toggle aria-controls="basic-navbar-nav">
            <span className="navbar-toggler-icon">
              {navExpand ? (
                <img
                  width="28"
                  height="28"
                  alt="Navbar Pink"
                  src="/images/nav-pink.svg"
                  className="pink"
                />
              ) : hamburgerIcon === 'white' ? (
                <img
                  width="28"
                  height="28"
                  alt="Navbar Pink"
                  src="/images/nav.svg"
                  className="white"
                />
              ) : (
                <img
                  width="28"
                  height="28"
                  alt="Navbar Pink"
                  src="/images/nav-pink.svg"
                  className="pink"
                />
              )}
            </span>
          </Navbar.Toggle>
          <Navbar.Collapse
            className={authenticated ? 'loged-in' : ''}
            id="basic-navbar-nav"
          >
            <Nav className="w-100">
              {activeLink !== 'For therapist' ? (
                <Nav.Item>
                  <Dropdown className="menu-icon">
                    <Dropdown.Toggle className="nav-link" as={CustomToggle}>
                      For therapist
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Link href="/become-a-partner">
                        <a className="dropdown-item">Become a Partner</a>
                      </Link>
                      <Link href="/become-a-partner#pricing">
                        <a className="dropdown-item">Pricing</a>
                      </Link>
                      <a className="dropdown-item" href={process.env.LOGIN_URL}>
                        Partner Login
                      </a>
                    </Dropdown.Menu>
                  </Dropdown>
                </Nav.Item>
              ) : (
                <>
                  <Nav.Item>
                    <a
                      className={cx('nav-link', {
                        active: 'Partner Login' === selectedLink,
                      })}
                      href={process.env.LOGIN_URL}
                    >
                      Partner Login
                    </a>
                  </Nav.Item>
                  <Nav.Item onClick={handleInternalNavigation}>
                    <Link href="#pricing">
                      <a
                        className={cx('nav-link', {
                          active: 'pricing' === selectedLink,
                        })}
                      >
                        Pricing
                      </a>
                    </Link>
                  </Nav.Item>
                </>
              )}
              <Nav.Item onClick={handleInternalNavigation}>
                <Link href={activeLink === 'For therapist' ? '#faq' : '/#faq'}>
                  <a
                    className={cx('nav-link', {
                      active: 'faq' === selectedLink,
                    })}
                  >
                    FAQ
                  </a>
                </Link>
              </Nav.Item>
              <Nav.Item>
                <Link href="/contact-us">
                  <a
                    className={cx('nav-link', {
                      active: 'Contact' === selectedLink,
                    })}
                  >
                    Contact
                  </a>
                </Link>
              </Nav.Item>
              {/* commented out temporarily */}
              {/* <Nav.Item>
              <Link href="">
                <a
                  className={cx('nav-link', {
                    active: 'Blog' === selectedLink,
                  })}
                >
                  Blog
                </a>
              </Link>
            </Nav.Item> */}

              {!authenticated ? (
                <>
                  <Nav.Item className="ml-auto">
                    <Link
                      href={`/login?redirectFrom=${
                        router?.pathname === '/login' ||
                        router?.pathname === '/signup'
                          ? !isEmpty(router.query) && router.query.redirectFrom
                            ? newAsPath
                            : '/'
                          : router?.asPath || '/'
                      }`}
                    >
                      <a
                        className={cx('nav-link', {
                          active: 'Login' === selectedLink,
                        })}
                        onClick={() => handleCollapse('login')}
                      >
                        Log in
                      </a>
                    </Link>
                  </Nav.Item>
                  <Nav.Item className="mr-0">
                    {activeLink === 'For therapist' ? (
                      <Link href="/request-demo">
                        <Button className="last-btn">Book A demo</Button>
                      </Link>
                    ) : (
                      <Link
                        href={`/signup?redirectFrom=${
                          router?.pathname === '/login' ||
                          router?.pathname === '/signup'
                            ? !isEmpty(router.query) &&
                              router.query.redirectFrom
                              ? newAsPath
                              : '/'
                            : router?.asPath || '/'
                        }`}
                      >
                        <a>
                          <Button
                            className="last-btn"
                            onClick={() => {
                              /* FBP signup */
                              // window.fbq('trackCustom', 'signup', {
                              //   actionItem: 'button',
                              //   data: null,
                              //   event_label: 'standard',
                              //   event_category: 'impressions',
                              // });

                              // /* Gtag signup */
                              // window.gtag('event', 'signup', {
                              //   event_category: 'impressions',
                              //   event_label: 'standard',
                              //   actionItem: 'button',
                              //   data: null,
                              // });
                              handleCollapse('signup');
                            }}
                          >
                            Sign up
                          </Button>
                        </a>
                      </Link>
                    )}
                  </Nav.Item>
                </>
              ) : (
                <>
                  <Nav.Item className="ml-auto user-profile">
                    {userRewards?.reward ? (
                      <div className="gift-img">
                        <div className="item" onClick={handleRedrect}>
                          <span className="notify-badge">
                            {userRewardsList?.length}
                          </span>
                          <Image
                            // layout="fill"
                            layout="fixed"
                            width={30}
                            height={30}
                            src="/images/gift56.png"
                          />
                        </div>
                        <Link
                          href="/promo-code"
                          onClick={() => handleToggle(false)}
                        >
                          {window.innerWidth >= 1024 ? (
                            <a>
                              Get $10 off
                              <span className="badge badge-pill badge-warning count-notif green">
                                New
                              </span>
                            </a>
                          ) : (
                            <a>
                              {' '}
                              {userRewards?.reward_text}
                              <span className="badge badge-pill badge-warning count-notif green">
                                New
                              </span>
                            </a>
                          )}
                        </Link>
                      </div>
                    ) : (
                      <div className="gift-img" onClick={handleRedrect}>
                        <Image
                          // layout="fill"
                          layout="fixed"
                          width={30}
                          height={30}
                          src="/images/gift56.png"
                        />

                        <Link
                          href="/promo-code"
                          onClick={() => handleToggle(false)}
                        >
                          {window.innerWidth >= 1024 &&
                          window.innerWidth <= 1280 ? (
                            <a>
                              Get $10 off
                              <span className="badge badge-pill badge-warning count-notif green">
                                New
                              </span>
                            </a>
                          ) : (
                            <a>
                              {' '}
                              {userRewards?.reward_text}
                              <span className="badge badge-pill badge-warning count-notif green">
                                New
                              </span>
                            </a>
                          )}
                        </Link>
                      </div>
                    )}
                  </Nav.Item>
                  <Nav.Item className="user-profile">
                    <Dropdown>
                      <Dropdown.Toggle as={CustomToggle}>
                        <span className="profile-img">
                          {!isEmpty(userProfile) && userProfile.avatar ? (
                            <img alt="User avatar" src={userProfile.avatar} />
                          ) : (
                            <img alt="User avatar" src="/images/avatar.png" />
                          )}
                        </span>
                        {!isEmpty(userProfile)
                          ? `${userProfile.firstname} ${userProfile.lastname}`
                          : 'User'}
                      </Dropdown.Toggle>

                      <Dropdown.Menu>
                        <div>
                          <Link href="/profile">
                            <a>Settings</a>
                          </Link>
                        </div>
                        <div>
                          <Link href="/order-history">
                            <a>Appointments</a>
                          </Link>
                        </div>
                        <div>
                          <Link href="/intake-form">
                            <a>Intake froms</a>
                          </Link>
                        </div>
                        <SeperatorStyle className="seperator" />
                        <div>
                          <a onClick={handleLogout}>Log out</a>
                        </div>
                      </Dropdown.Menu>
                    </Dropdown>
                  </Nav.Item>
                </>
              )}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </StyledHeader>
    );
  }
}

CustomHeader.propTypes = {
  className: PropTypes.string,
  hamburgerIcon: PropTypes.string,
  activeLink: PropTypes.string,
};
