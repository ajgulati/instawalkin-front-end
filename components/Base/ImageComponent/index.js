import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import { useImage } from 'react-image';
import { CustomImageComponentStyle } from '../../StyleComponent';
import Loader from 'react-loader-spinner';

function ImageSource({ src }) {
  const { src: srcList } = useImage({
    srcList: [src, '/images/avatar.png'],
  });

  return <img alt="user avatar" src={srcList} />;
}

ImageSource.propTypes = {
  src: PropTypes.string,
};

/**
 * Image Component with Loader
 * @param src - path of the image
 * @param props - rest of the props
 */
export default function CustomImageComponent({ src, ...props }) {
  return (
    <Suspense
      fallback={<Loader type="Oval" color="#ff7271" height={50} width={50} />}
    >
      <CustomImageComponentStyle {...props}>
        <ImageSource src={src} />
      </CustomImageComponentStyle>
    </Suspense>
  );
}

CustomImageComponent.propTypes = {
  src: PropTypes.string,
};
