import React from 'react';
import Modal from 'react-bootstrap/Modal';

/**
 * Custom bootstrap Modal
 * @param children - React node
 * @param visible - modal visibility boolean
 * @param props - rest of the props
 */
export default function CustomModal({
  className,
  visible,
  onHide,
  size,
  container,
  ...props
}) {
  return (
    <Modal
      size={size}
      aria-labelledby="contained-modal-title-vcenter"
      centered
      show={visible}
      className={className}
      onHide={onHide}
      container={container}
      style={{ zIndex: '3000' }}
      {...props}
    >
      <Modal.Header closeButton closeLabel="Close">
        {props.header}
      </Modal.Header>
      <Modal.Body>{props.body}</Modal.Body>
      <Modal.Footer>{props.footer}</Modal.Footer>
    </Modal>
  );
}
