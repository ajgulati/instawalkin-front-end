import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Link from 'next/link';
import { format, parseISO } from 'date-fns';
import isEmpty from 'lodash.isempty';
import Input from '../../Base/Input';
import Button from '../../Base/Button';
import { Button as TooltipBtn, Tooltip, OverlayTrigger } from 'react-bootstrap';
import { cartViewService, ctaRecordLogger } from 'services';
import { formatYearDate } from 'util/config';
import { MassageBoxStyleWrapper } from './style';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import GoogleMapsModal from '../../../containers/GoogleMapsModal';

// Tooltips for practice type icons
function renderTooltip(text) {
  return <Tooltip id="button-tooltip">{text.description}</Tooltip>;
}

/* Massage Therapist Card */
export const MassageTherapistBox = ({
  data = {},
  projectIdSpecific,
  router,
  datetime,
  isTime = false,
  locationValue,
  handleProfileClick,
  handleProfileNameClick,
  handleTimeClick,
  handleTelephoneClick,
  handleMainClick,
}) => (
  <React.Fragment>
    <div className="profile-name">
      <div className="img">
        <Link
          href={{
            pathname: `/massage-therapists/[city]/[specialist]`,
            query: {
              dateValue: datetime,
              projectId: projectIdSpecific,
              lat: locationValue?.geoLoc?.lattitude,
              lng: locationValue?.geoLoc?.longitude,
            },
          }}
          as={`/massage-therapists/${data.city}/${data.slug}?dateValue=${datetime}&projectId=${projectIdSpecific}&lat=${locationValue?.geoLoc?.lattitude}&lng=${locationValue?.geoLoc?.longitude}`}
        >
          <a onClick={handleProfileClick}>
            <img
              alt="RMT avatar"
              src={data?.mini_avatar || '/images/avatar.png'}
            />
          </a>
        </Link>
      </div>
      <div className="profile-info">
        <div className="name-rating">
          <Link
            href={{
              pathname: `/massage-therapists/[city]/[specialist]`,
              query: {
                dateValue: datetime,
                projectId: projectIdSpecific,
                lat: locationValue?.geoLoc?.lattitude,
                lng: locationValue?.geoLoc?.longitude,
              },
            }}
            as={`/massage-therapists/${data.city}/${data.slug}?dateValue=${datetime}&projectId=${projectIdSpecific}&lat=${locationValue?.geoLoc?.lattitude}&lng=${locationValue?.geoLoc?.longitude}`}
          >
            <a onClick={handleProfileNameClick}>
              <h4 className="sub-title">{`${data.manager_first_name}`}</h4>
            </a>
          </Link>
          <div className="rate">
            {data.rating && data.rating !== '0' && data.review_count != 0 && (
              <img
                width="12"
                height="12"
                alt="rating start"
                src="/images/star.svg"
              />
            )}
            <div>
              {data.rating &&
                data.rating !== '0' &&
                data.review_count != 0 &&
                data.rating + ' / 5'}{' '}
              <span>
                {data.review_count && data.rating !== '0'
                  ? '(' + data.review_count + ')'
                  : null}
              </span>
            </div>
          </div>
        </div>
        <div className="location">
          <div className="address">
            <GoogleMapsModal
              address={!isEmpty(data) ? data.address : 'Location'}
              lat={!isEmpty(data) ? data.position.coordinates[1] : null}
              lng={!isEmpty(data) ? data.position.coordinates[0] : null}
            />
          </div>
          <span className="km">{!isEmpty(data) ? data.distance : 0} km</span>
        </div>
      </div>
    </div>
    <ul className="list-unstyled facility">
      {data.parking && <li>Free parking</li>}
      {data.direct_billing ? (
        <li>Direct billing</li>
      ) : (
        <li> No direct billing</li>
      )}
    </ul>
    <ul className="list-unstyled massage-types " style={{ fontSize: '14px' }}>
      {!isEmpty(data.manager_specialities)
        ? data.manager_specialities.slice(0, 5).map((specialist) => (
            <li key={specialist.code}>
              <OverlayTrigger
                placement="bottom"
                overlay={renderTooltip(specialist)}
              >
                <span>
                  <img
                    alt="RMT Avatar"
                    src={specialist.image_path}
                    width="12"
                    height="12"
                  />
                  <span style={{ textTransform: 'capitalize' }}>
                    {' '}
                    {specialist.description.toLowerCase()}
                  </span>
                </span>
              </OverlayTrigger>
            </li>
          ))
        : null}
    </ul>
    {/* <p className="review">
      {data.tag_line.length > 100
        ? data.tag_line.substring(0, 100) + '...'
        : data.tag_line}
    </p> */}
    {isTime ? (
      <div
        className={classNames({
          'massage-list time-slot mt-auto': true,
          'mb-auto': data.availability.askedDateAvailable,
        })}
      >
        {data.availability.askedDateAvailable ? (
          data.availability.timeslots.map((time, index) => {
            if (index < 6) {
              return (
                <Link
                  key={index}
                  href={{
                    pathname: `/place-order/[order]`,
                    query: {
                      info: JSON.stringify({
                        time: time.display_time,
                        projectId: projectIdSpecific,
                        dateTime: datetime,
                        start: time.start,
                        end: time.end,
                        slug: data.slug,
                        prevPath: router.asPath,
                        city: data.city,
                      }),
                    },
                  }}
                  as={{
                    pathname: `/place-order/${data.slug}`,
                    query: {
                      info: JSON.stringify({
                        time: time.display_time,
                        projectId: projectIdSpecific,
                        dateTime: datetime,
                        start: time.start,
                        end: time.end,
                        slug: data.slug,
                        prevPath: router.asPath,
                        city: data.city,
                      }),
                    },
                  }}
                >
                  <a onClick={() => handleTimeClick(data, time)}>
                    <div className="badge">
                      <span>{time.display_time}</span>
                    </div>
                  </a>
                </Link>
              );
            } else {
              return null;
            }
          })
        ) : (
          <p className="active phone-number">
            <span>
              <i className="fas fa-exclamation-circle exclamation"></i>{' '}
              {!isEmpty(data.availability.daysAvailable)
                ? 'Next opening on ' +
                  format(parseISO(data.availability.daysAvailable[0]), 'd MMM')
                : 'No availability next 4 weeks'}{' '}
            </span>
          </p>
        )}
      </div>
    ) : (
      <div className="phone-number">
        {data.taking_clients ? (
          <a
            href={`${'tel:'}${data.phone}`}
            onClick={() => handleTelephoneClick(data)}
          >
            {data.phone
              ? data.phone.substring(0, 2) +
                '-(' +
                data.phone.substring(2, 5) +
                ')-' +
                data.phone.substring(5, 8) +
                '-' +
                data.phone.substring(8)
              : ''}
          </a>
        ) : (
          <span>
            {' '}
            <i className="fas fa-exclamation-circle exclamation"></i> Not taking
            clients currently.
          </span>
        )}
      </div>
    )}
    <Link
      href={{
        pathname: `/massage-therapists/[city]/[specialist]`,
        query: {
          dateValue: datetime,
          projectId: projectIdSpecific,
          lat: locationValue?.geoLoc?.lattitude,
          lng: locationValue?.geoLoc?.longitude,
        },
      }}
      as={`/massage-therapists/${data.city}/${data.slug}?dateValue=${datetime}&projectId=${projectIdSpecific}&lat=${locationValue?.geoLoc?.lattitude}&lng=${locationValue?.geoLoc?.longitude}`}
    >
      <Button
        className="w-100 btn-light-blue"
        onClick={() => handleMainClick(data, isTime)}
      >
        {isTime ? 'Find Available Time' : 'More info'}
      </Button>
    </Link>
  </React.Fragment>
);

/* Massage Therapist Recommend Form */
export const MassageBlackFormBox = ({ onSubmit }) => {
  const { register, handleSubmit, setValue, errors, reset } = useForm();
  return (
    <React.Fragment>
      <h3 className="box-title">Don’t see your Massage Therapist?</h3>
      <p>Add their information and we’ll contact them.</p>
      <form onSubmit={handleSubmit((data) => onSubmit(data, reset))}>
        <div className="input-group no-margin">
          <Input
            placeholder="Therapist Name"
            name="therapist_name"
            className="full-width"
            refProps={register({
              required: true,
              minLength: 5,
            })}
            isInvalid={
              !isEmpty(errors) && !isEmpty(errors.therapist_name) ? true : false
            }
            onBlur={(e) => {
              setValue('therapist_name', e.target.value.trim());
            }}
          />
          <div className="validation-container">
            {errors.therapist_name &&
              errors.therapist_name.type === 'required' && (
                <p className="error-message">Therapist name is required</p>
              )}
            {errors.therapist_name &&
              errors.therapist_name.type === 'minLength' && (
                <p className="error-message">
                  Must be at least 5 characters long
                </p>
              )}
          </div>
        </div>

        {/* <div className="input-group no-margin">
              <Input
                placeholder="Therapist Email"
                name="therapist_email"
                className="full-width"
                refProps={register({
                  pattern: emailRegex,
                })}
                onChange={(e) => {
                  let value = e.target.value;
                  if (/@/.test(value)) {
                    ctaRecordLogger({ email: value, type: 'L' });
                  }
                }}
                isInvalid={
                  !isEmpty(errors) && !isEmpty(errors.therapist_email)
                    ? true
                    : false
                }
              />
              <div className="validation-container">
                {errors.therapist_email &&
                  errors.therapist_email.type === 'pattern' && (
                    <p className="error-message">Invalid email</p>
                  )}
              </div>
            </div> */}

        <div className="input-group no-margin">
          <Input
            placeholder="Therapist Business or Email"
            name="therapist_business"
            className="full-width"
            refProps={register({
              minLength: 2,
            })}
            isInvalid={
              !isEmpty(errors) && !isEmpty(errors.therapist_business)
                ? true
                : false
            }
            onChange={(e) => {
              let value = e.target.value;
              if (/@/.test(value)) {
                ctaRecordLogger({ email: value, type: 'L' });
              }
            }}
          />
          <div className="validation-container">
            {errors.therapist_business &&
              errors.therapist_business.type === 'minLength' && (
                <p className="error-message">
                  Must be at least 2 characters long
                </p>
              )}
          </div>
        </div>

        <Button className="w-100" type="submit">
          Recommend
        </Button>
      </form>
    </React.Fragment>
  );
};

/* Massage Therapist Main Massage Box */
export default function MassageBoxComponent({
  className,
  data = {},
  projectIdSpecific,
  router,
  datetime,
  isTime = false,
  variant = 'box',
  userProfile,
  locationValue,
  recommendTherapist,
  ...props
}) {
  const onSubmit = (data, reset) => {
    // Remove empty fields from data object
    Object.keys(data).forEach((key) => {
      if (data[key] === '') {
        delete data[key];
      }
    });

    // Send data to backend
    recommendTherapist(data, reset);
  };

  const handleTimeClick = async (data, time) => {
    if (!isEmpty(userProfile)) {
      await cartViewService({
        booking_slug: data.slug,
        date_time: formatYearDate(time.start),
      });
    }

    window.dataLayer.push({
      event: 'addedToCart',
      event_label: data.slug,
      event_category: 'cart',
      actionItem: 'timeslot',
      data: null,
    });
  };

  const handleTelephoneClick = (data) => {
    window.dataLayer.push({
      event: 'phoneClick',
      event_label: data.slug,
      event_category: 'clicks',
      data: null,
    });
  };

  const handleProfileClick = () => {
    window.dataLayer.push({
      event: 'profilePictureClick',
      event_label: data.slug,
      event_category: 'clicks',
      data: null,
    });
  };

  const handleMainClick = (data, timeCheck) => {
    window.dataLayer.push({
      event: timeCheck ? 'MoreTimesClick' : 'MoreInfoClick',
      event_label: data.slug,
      event_category: 'clicks',
      data: null,
    });
  };

  const handleProfileNameClick = () => {
    window.dataLayer.push({
      event: 'profileNameClick',
      event_label: data.slug,
      event_category: 'clicks',
      data: null,
    });
  };

  return (
    <MassageBoxStyleWrapper
      {...props}
      className={cx(className, {
        box: variant !== 'box-black',
        'box-black': variant === 'box-black',
      })}
    >
      {variant === 'box' ? (
        <MassageTherapistBox
          data={data}
          projectIdSpecific={projectIdSpecific}
          router={router}
          datetime={datetime}
          isTime={isTime}
          locationValue={locationValue}
          handleProfileClick={handleProfileClick}
          handleProfileNameClick={handleProfileNameClick}
          handleTimeClick={handleTimeClick}
          handleTelephoneClick={handleTelephoneClick}
          handleMainClick={handleMainClick}
        />
      ) : (
        <MassageBlackFormBox onSubmit={onSubmit} />
      )}
    </MassageBoxStyleWrapper>
  );
}

MassageBoxComponent.propTypes = {
  className: PropTypes.string,
  datetime: PropTypes.string,
  projectIdSpecific: PropTypes.number,
  variant: PropTypes.string,
  data: PropTypes.object,
  locationValue: PropTypes.object,
  userProfile: PropTypes.object,
  router: PropTypes.object,
  isTime: PropTypes.bool,
  recommendTherapist: PropTypes.func,
  handleMoreTime: PropTypes.func,
};
