import React from 'react';
import styled from 'styled-components';

const StyledBar = styled.div`
  background: ${(props) => props.bgColor};
  color: ${(props) => props.textColor};
  text-align: center;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 18px;
  font-size: 1em;

  p {
    margin: 0;
    padding: 0.75em;
  }
`;

const MessageBar = ({ message, bgColor = '#3ed590', textColor = '#fff' }) => {
  return (
    <StyledBar bgColor={bgColor} textColor={textColor}>
      <p>{message}</p>
    </StyledBar>
  );
};

export default MessageBar;
