import React from 'react';
import PropTypes from 'prop-types';
import Loader from 'react-loader-spinner';

export default function PreLoadComponent({ className, ...props }) {
  return (
    <div {...props}>
      <Loader type="Puff" color="#ff7271" height={75} width={75} />
    </div>
  );
}

PreLoadComponent.propTypes = {
  className: PropTypes.string,
};
