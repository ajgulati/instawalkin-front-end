import React from 'react';
import PropTypes from 'prop-types';
import Alert from 'react-bootstrap/Alert';
import { motion, AnimatePresence } from 'framer-motion';
import cx from 'classnames';
export default function CustomSuccessAlert({ message, className }) {
  return (
    <AnimatePresence>
      {message && (
        <motion.div
          className={cx(
            'alert-global-success-wrapper success-alert',
            className
          )}
          transition={{ duration: 0.3 }}
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
        >
          <Alert variant={'success'}>{message}</Alert>
        </motion.div>
      )}
    </AnimatePresence>
  );
}

CustomSuccessAlert.propTypes = {
  message: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  className: PropTypes.string,
};
