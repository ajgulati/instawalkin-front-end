## Introduction

The Project is bootstrapped with _create-next-app_ + _storybook_.

## Getting Started

Installing Dependency

```bash
npm install
# or
yarn install
```

For Starting Nextjs Server

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

For the production server:

```bash
npm run build
# or
yarn build

npm run start
# or
yarn start
```

For Starting Storybook Server

```bash
npm run storybook
# or
yarn storybook
```

## Modules

- Nextjs
- Storybook
- Styled-components
- Redux
- dotenv


## For Production Server Deployment

```bash
npm install
npm run build
```

## Compress package.json, .next, public and next.config.js into one file and deploy it.