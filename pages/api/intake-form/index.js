import axios from 'axios';
const VERSION = process.env.BASE_VERSION;
const PREFIX = process.env.BASE_PREFIX;
const BASE = process.env.API_URL;

export default async (req, res) => {
  const { requestPayload } = req.body;
  let payload = {
    name: requestPayload.name,
    address: requestPayload.address,
    phone: requestPayload.phone,
    birthdate: requestPayload.birthdate,
    medical_conditions: JSON.stringify(requestPayload.medical_conditions),
    care: JSON.stringify(requestPayload.care),
    surgery: JSON.stringify(requestPayload.surgery),
    fractures: JSON.stringify(requestPayload.fractures),
    illness: JSON.stringify(requestPayload.illness),
    motor_workplace: JSON.stringify(requestPayload.motor_workplace),
    tests: JSON.stringify(requestPayload.tests),
    modifier: 'U',
    active: 1,
    consent: 1,
  };
  if (requestPayload.referred_by)
    payload['referred_by'] = requestPayload.referred_by;
  if (requestPayload.physician_name)
    payload['physician_name'] = requestPayload.physician_name;
  if (requestPayload.allergies) payload['allergies'] = requestPayload.allergies;
  if (requestPayload.current_medications)
    payload['current_medications'] = requestPayload.current_medications;
  if (requestPayload.relieves) payload['relieves'] = requestPayload.relieves;
  if (requestPayload.aggravates)
    payload['aggravates'] = requestPayload.aggravates;
  if (requestPayload.sports_activities)
    payload['sports_activities'] = requestPayload.sports_activities;
  try {
    const response = await axios({
      method: 'POST',
      url: `${BASE}/${PREFIX}/${VERSION}/intake-forms`,
      headers: {
        'content-type': 'application/json',
        accept: 'application/json',
        Authorization: req.headers.authorization,
      },
      data: {
        ...payload,
      },
    });
    return res.status(200).json(response.data);
  } catch (e) {
    console.log('ERROR: ', e.response);
    if (e.response.data) {
      return res.status(e.response.status).json({ ...e.response.data });
    }
    return res
      .status(200)
      .json({ status: false, errors: 'Internal Server Error' });
  }
};

export const config = {
  api: {
    bodyParser: {
      sizeLimit: '1mb',
    },
  },
};
