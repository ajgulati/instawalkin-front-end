export default async (req, res) => {
  try {
    const { queryParams } = req.body;
    const response = queryParams;

    if (response) {
      let payload = {
        name: response?.name,
        address: response?.address,
        phone: response?.phone,
        birthdate: response?.birthdate,
        medical_conditions: response?.medical_conditions,
        care: response?.care,
        surgery: response?.surgery,
        fractures: response?.fractures,
        illness: response?.illness,
        motor_workplace: response?.motor_workplace,
        tests: response?.tests,
        status: true,
      };
      if (response?.referred_by) payload['referred_by'] = response?.referred_by;
      if (response?.physician_name)
        payload['physician_name'] = response?.physician_name;
      if (response?.allergies) payload['allergies'] = response?.allergies;
      if (response?.current_medications)
        payload['current_medications'] = response?.current_medications;
      if (response?.relieves) payload['relieves'] = response?.relieves;
      if (response?.aggravates) payload['aggravates'] = response?.aggravates;
      if (response?.sports_activities)
        payload['sports_activities'] = response?.sports_activities;
      return res.status(200).json(payload);
    }
  } catch (e) {
    console.log('ERROR: ', e.response);
    if (e.response.data) {
      return res.status(e.response.status).json({ ...e.response.data });
    }
    return res
      .status(200)
      .json({ status: false, errors: 'Internal Server Error' });
  }
};

export const config = {
  api: {
    bodyParser: {
      sizeLimit: '1mb',
    },
  },
};
