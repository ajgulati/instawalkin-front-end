import axios from 'axios';
const VERSION = process.env.BASE_VERSION;
const PREFIX = process.env.BASE_PREFIX;
const BASE = process.env.API_URL;

export default async (req, res) => {
  try {
    const { id } = req.body;
    const response = await axios({
      method: 'GET',
      url: `${BASE}/${PREFIX}/${VERSION}/intake-forms/${id}`,
      headers: {
        'content-type': 'application/json',
        accept: 'application/json',
        Authorization: req.headers.authorization,
      },
    });
    if (response) {
      let payload = {
        name: response.data.data.name,
        address: response.data.data.address,
        phone: response.data.data.phone,
        birthdate: response.data.data.birthdate,
        medical_conditions: response.data.data.medical_conditions,
        care: response.data.data.care,
        surgery: response.data.data.surgery,
        fractures: response.data.data.fractures,
        illness: response.data.data.illness,
        motor_workplace: response.data.data.motor_workplace,
        tests: response.data.data.tests,
        status: true,
      };
      if (response.data.data.referred_by)
        payload['referred_by'] = response.data.data.referred_by;
      if (response.data.data.physician_name)
        payload['physician_name'] = response.data.data.physician_name;
      if (response.data.data.allergies)
        payload['allergies'] = response.data.data.allergies;
      if (response.data.data.current_medications)
        payload['current_medications'] = response.data.data.current_medications;
      if (response.data.data.relieves)
        payload['relieves'] = response.data.data.relieves;
      if (response.data.data.aggravates)
        payload['aggravates'] = response.data.data.aggravates;
      if (response.data.data.sports_activities)
        payload['sports_activities'] = response.data.data.sports_activities;
      return res.status(200).json(payload);
    }
  } catch (e) {
    console.log('ERROR: ', e.response);
    if (e.response.data) {
      return res.status(e.response.status).json({ ...e.response.data });
    }
    return res
      .status(200)
      .json({ status: false, errors: 'Internal Server Error' });
  }
};

export const config = {
  api: {
    bodyParser: {
      sizeLimit: '1mb',
    },
  },
};
