import axios from 'axios';

export default async (req, res) => {
  const { payload, query } = req.body;
  let data = {
    name: payload.name,
    testing: JSON.stringify(payload.testing),
    exposure: JSON.stringify(payload.exposure),
    travel: JSON.stringify(payload.travel),
    symptoms: JSON.stringify(payload.symptoms),
    contact: JSON.stringify(payload.contact),
    actions: JSON.stringify(payload.actions),
    modifier: 'U',
    active: 1,
    consent: 1,
  };
  if (payload.precautions) data['precautions'] = payload.precautions;

  try {
    let response = await axios({
      method: 'POST',
      url: query.url,
      headers: {
        'content-type': 'application/json',
        accept: 'application/json',
      },
      data: {
        ...data,
      },
    });

    return res.status(200).json(response.data);
  } catch (e) {
    console.log('ERROR: ', e.response);
    if (e.response.data) {
      return res.status(e.response.status).json({ ...e.response.data });
    }
    return res
      .status(200)
      .json({ status: false, errors: 'Internal Server Error' });
  }
};

export const config = {
  api: {
    bodyParser: {
      sizeLimit: '1mb',
    },
  },
};
