export default async (req, res) => {
  try {
    const { queryParams } = req.body;
    const response = queryParams;

    if (response) {
      let payload = {
        actions: response?.actions,
        contact: response?.contact,
        exposure: response?.exposure,
        name: response?.name,
        symptoms: JSON.parse(response?.symptoms),
        testing: JSON.parse(response?.testing),
        travel: JSON.parse(response?.travel),
      };

      if (response?.precautions) payload['precautions'] = response?.precautions;

      if (payload?.status) payload['status'] = response?.status;

      return res.status(200).json(payload);
    }
  } catch (e) {
    console.log('ERROR: ', e.response);
    if (e.response.data) {
      return res.status(e.response.status).json({ ...e.response.data });
    }
    return res
      .status(200)
      .json({ status: false, errors: 'Internal Server Error' });
  }
};

export const config = {
  api: {
    bodyParser: {
      sizeLimit: '1mb',
    },
  },
};
