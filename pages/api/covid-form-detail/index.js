import axios from 'axios';
const VERSION = process.env.BASE_VERSION;
const PREFIX = process.env.BASE_PREFIX;
const BASE = process.env.API_URL;

export default async (req, res) => {
  try {
    const { id } = req.body;
    const response = await axios({
      method: 'GET',
      url: `${BASE}/${PREFIX}/${VERSION}/covid-forms/${id}`,
      headers: {
        'content-type': 'application/json',
        accept: 'application/json',
        Authorization: req.headers.authorization,
      },
    });
    if (response) {
      console.log(response);
    }
  } catch (e) {
    if (e.response.data) {
      return res.status(e.response.status).json({ ...e.response.data });
    }
    return res
      .status(200)
      .json({ status: false, errors: 'Internal Server Error' });
  }
};

export const config = {
  api: {
    bodyParser: {
      sizeLimit: '1mb',
    },
  },
};
