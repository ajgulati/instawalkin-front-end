import MassagePractice from 'containers/MassagePractice';
import { getProjectPerTherapist, getTeamListedTherapists } from 'services';
import { statusCheck, formatDate } from 'util/config';

export const getServerSideProps = async (ctx) => {
  const { params, query } = ctx;
  try {
    let responseProject = await getProjectPerTherapist({
      slug: params.specialist ? params.specialist : '',
    });

    if (statusCheck(responseProject)) {
      let defaultExist = false,
        projectId = 0,
        mainData = {};
      responseProject.data.data.map((element) => {
        if (element.default) {
          defaultExist = true;
          projectId = element.id;
        }
      });
      if (!defaultExist) {
        projectId = responseProject.data.data[0].id;
      }
      if (projectId) {
        let response = await getTeamListedTherapists(
          {
            city: params.city,
            team_slug: params.team_slug,
            date: formatDate(new Date()),
            projectId: query.projectId ? +query.projectId : projectId,
          },
          true
        );
        mainData = Array.isArray(response.data)
          ? response.data[0]
          : response.data;
      }
      return {
        props: {
          practiceInfo: mainData.practice_info,
        },
      };
    }
    return {
      props: {
        practiceInfo: {},
      },
    };
  } catch (error) {
    return {
      props: {
        practiceInfo: {},
      },
    };
  }
};

export default MassagePractice;
