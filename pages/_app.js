import React from 'react';
import App from 'next/app';
import { ThemeProvider } from 'styled-components';
import { Router } from 'next/router';
import { Provider } from 'react-redux';
import Head from 'next/head';
import * as Sentry from '@sentry/browser';
import PageLoader from 'components/Views/PageLoader';
import { theme } from 'util/config';
import withReduxStore from 'redux/withReduxStore';
import AppGlobalComponent from 'containers/App';
import 'react-datepicker/dist/react-datepicker.css';
import '@fortawesome/fontawesome-free/css/all.css';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import 'assets/styles/main.scss';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';

Sentry.init({ dsn: process.env.SENTRY_DNS_KEY });

class MyApp extends App {
  state = {
    routePageLoading: false,
    stripePromise: null,
  };

  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }
  componentWillMount() {
    this.setState({
      ...this.state,
      stripePromise: loadStripe(process.env.STRIPE_KEY),
    });
  }
  componentDidMount() {
    const handleRouteChangeStart = () =>
      this.setState({ ...this.state, routePageLoading: true });
    const handleRouteChangeComplete = () =>
      this.setState({ ...this.state, routePageLoading: false });

    /**
     * Route events to add PageLoader between pages.
     */
    Router.events.on('routeChangeStart', handleRouteChangeStart);
    Router.events.on('routeChangeComplete', handleRouteChangeComplete);
    Router.events.on('routeChangeError', handleRouteChangeComplete);
  }

  componentDidCatch(error, errorInfo) {
    /**
     * Catch all error and pass it to sentry.
     */
    Sentry.withScope((scope) => {
      Object.keys(errorInfo).forEach((key) => {
        scope.setExtra(key, errorInfo[key]);
      });

      Sentry.captureException(error);
    });

    super.componentDidCatch(error, errorInfo);
  }

  /**
   * Meta Tag implementation accourding to the props change.
   */
  metaTagReturn(pP) {
    if (pP?.massageData?.profile_photo) {
      return (
        <meta property="og:image" content={pP?.massageData?.profile_photo} />
      );
    } else {
      return (
        <>
          <meta
            property="og:image"
            content={`${process.env.WEBAPP_URL}/images/Thrivr_2.png`}
          />
          <link rel="canonical" href={`${process.env.WEBAPP_URL}`} />

          <link rel="icon" href="/favicon.ico" />

          <meta property="og:type" content="website" />
          <meta
            name="description"
            content="Find and book professional massage therapists on-demand in Saskatoon, Saskatchewan."
          />
        </>
      );
    }
  }
  render() {
    const { Component, pageProps, store } = this.props;
    return (
      <>
        <Head>
          <link rel="icon" href="/favicon.ico" />

          <meta property="og:site_name" content="Thrivr" />
          <meta property="fb:app_id" content="742729032963071" />
          <meta property="og:image:type" content="image/png" />
          <meta name="theme-color" content="#ff7271" />
          <link rel="manifest" href="/manifest.json" />
          <link
            rel="apple-touch-icon"
            sizes="57x57"
            href="/icon/apple-icon-57x57.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="60x60"
            href="/icon/apple-icon-60x60.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="72x72"
            href="/icon/apple-icon-72x72.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="76x76"
            href="/icon/apple-icon-76x76.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="192x192"
            href="/icon/apple-icon-192x192.png"
          />
          {this.metaTagReturn(pageProps)}

          <link
            rel="preload"
            href="/fonts/Muli-Black.ttf"
            as="font"
            type="font/ttf"
            crossOrigin="true"
          />
          <link
            rel="preload"
            href="/fonts/Muli-Bold.ttf"
            as="font"
            type="font/ttf"
            crossOrigin="true"
          />
          <link
            rel="preload"
            href="/fonts/Muli-Regular.ttf"
            as="font"
            type="font/ttf"
            crossOrigin="true"
          />
          <link
            rel="preload"
            href="/fonts/Muli-SemiBold.ttf"
            as="font"
            type="font/ttf"
            crossOrigin="true"
          />
        </Head>
        <Elements stripe={this.state.stripePromise}>
          <ThemeProvider theme={theme}>
            <Provider store={store}>
              <PageLoader visible={this.state.routePageLoading} />
              <Component {...pageProps} />
              {/**
               * Global App Component that will used on every page.
               */}
              <AppGlobalComponent />
            </Provider>
          </ThemeProvider>
        </Elements>
      </>
    );
  }
}

export default withReduxStore(MyApp);
